clc;
close all;
clear all;

%Simulation parameters
realizations = 1000;

plan = csvread("a3.csv");
plan = plan;

%Make walls from plan
wallx = zeros(size(plan,1)*3);
wally = zeros(size(plan,1)*3);
%make walls var
for i = 0:size(plan,1)-1
    wallx((i*3)+1) = plan(i+1,1);
    wallx((i*3)+2) = plan(i+1,3);
    wallx((i*3)+3) = NaN;
    wally((i*3)+1) = plan(i+1,2);
    wally((i*3)+2) = plan(i+1,4);
    wally((i*3)+3) = NaN;
end

%define areas in which points should be randomized
rooms = [   0 0 2.98 6.1            1;
    2.98 0 6.01 6.1         2;
    6.01 0 9.04 6.1         3;
    9.04 0 12.01 6.1        4;
    12.01 0 16.01 6.1       5;
    16.01 0 19.88 6.1       6;
    22.01 0 24.14 6.1       7;
    0 8.08 2.98 12.01       8;
    2.98 8.08 6.01 12.01    9;
    6.01 8.08 9.04 12.01    10;
    9.04 8.08 12.01 12.01   11;
    12.01 12.01 16.01 15.03 12;
    12.01 15.03 16.01 18.16 13;
    12.01 18.16 16.01 21.13 14;
    12.01 21.13 16.01 24.15 15;
    18.07 9 24.14 13.2      16;
    18.07 13.2 24.14 15.03  17;
    18.07 15.03 24.14 18.08 18;
    18.07 18.08 24.14 21.03 19;
    18.07 21.03 24.14 24.15 20];

%Make and define rooms
for i = 1:size(rooms,1)
    pgon(i) = polyshape([rooms(i,1) rooms(i,1) rooms(i,3) rooms(i,3)],[rooms(i,4) rooms(i,2) rooms(i,2) rooms(i,4)]);
end


%Generate beacon(CU) and sensor pairs
beaconx = zeros(size(rooms,1),realizations);
beacony = zeros(size(rooms,1),realizations);
sensorx = zeros(size(rooms,1),realizations);
sensory = zeros(size(rooms,1),realizations);

%Additional static beacon positions
beaconx_center = zeros(size(rooms,1),realizations);
beacony_center = zeros(size(rooms,1),realizations);
beaconx_corner = zeros(size(rooms,1),realizations);
beacony_corner = zeros(size(rooms,1),realizations);
for monte = 1:realizations
    beaconx_center(:,monte) = ((rooms(:,3)-rooms(:,1))/2)+rooms(:,1);
    beacony_center(:,monte) = ((rooms(:,4)-rooms(:,2))/2)+rooms(:,2);
end
offset = 0.3;
corner_x = [2.9800-offset
            6.0100-offset
            9.0400-offset
           12.0100-offset
           16.0100-offset
           19.8800-offset
           24.1400-offset
            0+offset
            2.9800+offset
            6.0100+offset
            9.0400+offset
           16.0100-offset
           16.0100-offset
           16.0100-offset
           16.0100-offset
           18.0700+offset
           18.0700+offset
           18.0700+offset
           18.0700+offset
           18.0700+offset];
       
corner_y = [ 6.1000-offset
            6.1000-offset
            6.1000-offset
            6.1000-offset
            6.1000-offset
            6.1000-offset
            6.1000-offset
            8.0800+offset
            8.0800+offset
            8.0800+offset
            8.0800+offset
           12.0100+offset
           15.0300+offset
           18.1600+offset
           21.1300+offset
           13.2000-offset
           15.0300-offset
           18.0800-offset
           21.0300-offset
           24.1500-offset];
       
for monte = 1:realizations
    beaconx_corner(:,monte) = corner_x;
    beacony_corner(:,monte) = corner_y;
end

%find distance between sensor and beacon
dist = zeros(size(rooms,1),size(rooms,1));
dist_center = zeros(size(rooms,1),size(rooms,1));
dist_corner = zeros(size(rooms,1),size(rooms,1));
rssi_clean = zeros(size(rooms,1),size(rooms,1),realizations);
rssi_clean_center = zeros(size(rooms,1),size(rooms,1),realizations);
rssi_clean_corner = zeros(size(rooms,1),size(rooms,1),realizations);
rssi_test = zeros(size(rooms,1),size(rooms,1),realizations);
n = zeros(size(rooms,1),size(rooms,1),realizations);
n_center = zeros(size(rooms,1),size(rooms,1),realizations);
n_corner = zeros(size(rooms,1),size(rooms,1),realizations);

%For every wanted realization in the monte carlo
for monte = 1:realizations
    hold on
    for i = 1:size(rooms,1)
        beaconx(i,monte) = rooms(i,1)+ (rooms(i,3)-rooms(i,1))*rand(1,1);
        beacony(i,monte) = rooms(i,2)+ (rooms(i,4)-rooms(i,2))*rand(1,1);
        
        sensorx(i,monte) = rooms(i,1)+ (rooms(i,3)-rooms(i,1))*rand(1,1);
        sensory(i,monte) = rooms(i,2)+ (rooms(i,4)-rooms(i,2))*rand(1,1);
    end
    
    %find distance between sensor and beacon
    for i = 1:size(rooms,1)
        for j = 1:size(rooms,1)
            dist(i,j) = sqrt((beaconx(j,monte)-sensorx(i,monte)).^2+(beacony(j,monte)-sensory(i,monte)).^2);
            
            dist_center(i,j) = sqrt((beaconx_center(j,monte)-sensorx(i,monte)).^2+(beacony_center(j,monte)-sensory(i,monte)).^2);
            dist_corner(i,j) = sqrt((beaconx_corner(j,monte)-sensorx(i,monte)).^2+(beacony_corner(j,monte)-sensory(i,monte)).^2);
            
            [XI, YI] = polyxpoly(wallx, wally, [sensorx(i,monte) beaconx(j,monte)],[sensory(i,monte) beacony(j,monte)]);
            n(i,j,monte) = length(XI);
            
            [XI_center, YI_center] = polyxpoly(wallx, wally, [sensorx(i,monte) beaconx_center(j,monte)],[sensory(i,monte) beacony_center(j,monte)]);
            n_center(i,j,monte) = length(XI_center);
            
            [XI_corner, YI_corner] = polyxpoly(wallx, wally, [sensorx(i,monte) beaconx_corner(j,monte)],[sensory(i,monte) beacony_corner(j,monte)]);
            n_corner(i,j,monte) = length(XI_corner);
            
            %lige her er den grande formel!!!!!!!!!!!!
            %rssi(i,j) = n;%
            rssi_clean(i,j,monte)=((-41.9813) + (10*(-2.055)*log10(dist(i,j)/0.5)));
            
            rssi_clean_center(i,j,monte)=((-41.9813) + (10*(-2.055)*log10(dist_center(i,j)/0.5)));
            
            rssi_clean_corner(i,j,monte)=((-41.9813) + (10*(-2.055)*log10(dist_corner(i,j)/0.5)));
        end
    end
    disp('generaiton:')
    disp(monte/realizations)
end

%Add noise to the RSSI measurements
noise = randn(size(rooms,1),size(rooms,1),realizations).*sqrt(22.159);
%rssi=rssi_clean;%+noise;


loss = 2;
%loss = 1;

ratio = zeros(realizations,length(loss));
ratio_noise = zeros(realizations,length(loss));
correct = zeros(realizations,length(loss));
correct_center = zeros(realizations,length(loss));
correct_corner = zeros(realizations,length(loss));
%% random
incl_ctrl_nr=2:10; %number of controllers to include in calculation


high_correct=zeros(1,length(incl_ctrl_nr));

    for z=1:length(incl_ctrl_nr)
        rssi = rssi_clean - (n.*loss)+ noise;

        esti_room=zeros(1,size(rssi,1));
        ctrl_err=zeros(1,incl_ctrl_nr(z));
        err=zeros(1,incl_ctrl_nr(z));
        for monte =1:realizations
            temp = 0;
            for sensor_nr=1:size(rssi,1) %Test for sensor number
                [B,I]=maxk(rssi(sensor_nr,:,monte),incl_ctrl_nr(z)); %Find the controllers with the highest RSSI to the sensor
                for room_nr=1:length(B) %Calculate the error for a given room

                    room_center=[(rooms(I(room_nr),1)+rooms(I(room_nr),3))/2 (rooms(I(room_nr),2)+rooms(I(room_nr),4))/2];

                    for ctrl_nr=1:length(B) %test for the controllers
                        % find dist
                        % max
                        controller_room_corner_dist=[sqrt((rooms(I(room_nr),1)-beaconx(I(ctrl_nr),monte))^2+(rooms(I(room_nr),2)-beacony(I(ctrl_nr),monte))^2)
                                                     sqrt((rooms(I(room_nr),1)-beaconx(I(ctrl_nr),monte))^2+(rooms(I(room_nr),4)-beacony(I(ctrl_nr),monte))^2)
                                                     sqrt((rooms(I(room_nr),3)-beaconx(I(ctrl_nr),monte))^2+(rooms(I(room_nr),2)-beacony(I(ctrl_nr),monte))^2)
                                                     sqrt((rooms(I(room_nr),3)-beaconx(I(ctrl_nr),monte))^2+(rooms(I(room_nr),4)-beacony(I(ctrl_nr),monte))^2)];
                        controller_room_dist_max=max(controller_room_corner_dist);

                        % min
                        if beaconx(I(ctrl_nr),monte) > rooms(I(room_nr),1) && beaconx(I(ctrl_nr),monte) < rooms(I(room_nr),3) && beacony(I(ctrl_nr),monte) > rooms(I(room_nr),2) && beacony(I(ctrl_nr),monte) < rooms(I(room_nr),4)
                            controller_room_dist_min=0;
                        elseif beaconx(I(ctrl_nr),monte) > rooms(I(room_nr),1) && beaconx(I(ctrl_nr),monte) < rooms(I(room_nr),3)
                            ywalldist=[abs(beacony(I(ctrl_nr),monte)-rooms(I(room_nr),2))
                                       abs(beacony(I(ctrl_nr),monte)-rooms(I(room_nr),4))];
                            controller_room_dist_min=min(ywalldist);
                        elseif beacony(I(ctrl_nr),monte) > rooms(I(room_nr),2) && beacony(I(ctrl_nr),monte) < rooms(I(room_nr),2)
                            xwalldist=[abs(beaconx(I(ctrl_nr),monte)-rooms(I(room_nr),1))
                                       abs(beaconx(I(ctrl_nr),monte)-rooms(I(room_nr),3))];
                            controller_room_dist_min=min(xwalldist);       
                        else
                            controller_room_dist_min=min(controller_room_corner_dist);
                        end

                        % number of walls between center of room and controller
                        [XI, YI] = polyxpoly(wallx, wally, [room_center(1) beaconx(I(ctrl_nr),monte)],[room_center(2) beacony(I(ctrl_nr),monte)]);
                        n(ctrl_nr) = length(XI);
                        rssi_wall_est=B(ctrl_nr)+n(ctrl_nr)*loss; %subtract estimated wall loss from rssi
                        ctrl_sens_esti_dist = 0.5*exp(-0.1120479364*rssi_wall_est - 4.703918033); %Find estimated sensor controller distance

                        if ctrl_sens_esti_dist > controller_room_dist_max
                            ctrl_err(ctrl_nr)= ctrl_sens_esti_dist - controller_room_dist_max;
                        elseif ctrl_sens_esti_dist < controller_room_dist_min
                            ctrl_err(ctrl_nr)= controller_room_dist_min - ctrl_sens_esti_dist;
                        else
                            ctrl_err(ctrl_nr)=0;
                        end

                    end
                    err(room_nr) = sqrt(sum(ctrl_err.^2));
                end
                [M,In]=min(err);
                esti_room(sensor_nr)=I(In);

            end
            for i=1:size(esti_room,2)
                if esti_room(i)==i
                    high_correct(z) = high_correct(z) + 1;
                    temp = temp + 1;
                end
            end
            correct(monte,z) = temp/20;
            ratio(monte,z) = high_correct(z)/(monte*20);
            disp('Random')
            disp(incl_ctrl_nr(z))
            disp(monte/realizations)
        end
    end

%% center
%number of controllers to include in calculation


high_correct=zeros(1,length(incl_ctrl_nr));


for z=1:length(incl_ctrl_nr)
    rssi = rssi_clean_center-(n_center.*loss)+noise;
    
    esti_room=zeros(1,size(rssi,1));
    ctrl_err=zeros(1,incl_ctrl_nr(z));
    err=zeros(1,incl_ctrl_nr(z));
    for monte =1:realizations
        temp = 0;
        for sensor_nr=1:size(rssi,1) %Test for sensor number
            [B,I]=maxk(rssi(sensor_nr,:,monte),incl_ctrl_nr(z)); %Find the controllers with the highest RSSI to the sensor
            for room_nr=1:length(B) %Calculate the error for a given room

                room_center=[(rooms(I(room_nr),1)+rooms(I(room_nr),3))/2 (rooms(I(room_nr),2)+rooms(I(room_nr),4))/2];

                for ctrl_nr=1:length(B) %test for the controllers
                    % find dist
                    % max
                    controller_room_corner_dist=[sqrt((rooms(I(room_nr),1)-beaconx_center(I(ctrl_nr),monte))^2+(rooms(I(room_nr),2)-beacony_center(I(ctrl_nr),monte))^2)
                                                 sqrt((rooms(I(room_nr),1)-beaconx_center(I(ctrl_nr),monte))^2+(rooms(I(room_nr),4)-beacony_center(I(ctrl_nr),monte))^2)
                                                 sqrt((rooms(I(room_nr),3)-beaconx_center(I(ctrl_nr),monte))^2+(rooms(I(room_nr),2)-beacony_center(I(ctrl_nr),monte))^2)
                                                 sqrt((rooms(I(room_nr),3)-beaconx_center(I(ctrl_nr),monte))^2+(rooms(I(room_nr),4)-beacony_center(I(ctrl_nr),monte))^2)];
                    controller_room_dist_max=max(controller_room_corner_dist);

                    % min
                    if beaconx_center(I(ctrl_nr),monte) > rooms(I(room_nr),1) && beaconx_center(I(ctrl_nr),monte) < rooms(I(room_nr),3) && beacony_center(I(ctrl_nr),monte) > rooms(I(room_nr),2) && beacony_center(I(ctrl_nr),monte) < rooms(I(room_nr),4)
                        controller_room_dist_min=0;
                    elseif beaconx_center(I(ctrl_nr),monte) > rooms(I(room_nr),1) && beaconx_center(I(ctrl_nr),monte) < rooms(I(room_nr),3)
                        ywalldist=[abs(beacony_center(I(ctrl_nr),monte)-rooms(I(room_nr),2))
                                   abs(beacony_center(I(ctrl_nr),monte)-rooms(I(room_nr),4))];
                        controller_room_dist_min=min(ywalldist);
                    elseif beacony_center(I(ctrl_nr),monte) > rooms(I(room_nr),2) && beacony_center(I(ctrl_nr),monte) < rooms(I(room_nr),2)
                        xwalldist=[abs(beaconx_center(I(ctrl_nr),monte)-rooms(I(room_nr),1))
                                   abs(beaconx_center(I(ctrl_nr),monte)-rooms(I(room_nr),3))];
                        controller_room_dist_min=min(xwalldist);       
                    else
                        controller_room_dist_min=min(controller_room_corner_dist);
                    end

                    % number of walls between center of room and controller
                    [XI, YI] = polyxpoly(wallx, wally, [room_center(1) beaconx_center(I(ctrl_nr),monte)],[room_center(2) beacony_center(I(ctrl_nr),monte)]);
                    n(ctrl_nr) = length(XI);
                    rssi_wall_est=B(ctrl_nr)+n(ctrl_nr)*loss; %subtract estimated wall loss from rssi
                    ctrl_sens_esti_dist = 0.5*exp(-0.1120479364*rssi_wall_est - 4.703918033); %Find estimated sensor controller distance

                    if ctrl_sens_esti_dist > controller_room_dist_max
                        ctrl_err(ctrl_nr)= ctrl_sens_esti_dist - controller_room_dist_max;
                    elseif ctrl_sens_esti_dist < controller_room_dist_min
                        ctrl_err(ctrl_nr)= controller_room_dist_min - ctrl_sens_esti_dist;
                    else
                        ctrl_err(ctrl_nr)=0;
                    end

                end
                err(room_nr) = sqrt(sum(ctrl_err.^2));
            end
            [M,In]=min(err);
            esti_room(sensor_nr)=I(In);

        end
        for i=1:size(esti_room,2)
            if esti_room(i)==i
                high_correct(z) = high_correct(z) + 1;
                temp = temp + 1;
            end
        end
        correct_center(monte,z) = temp/20;
        ratio(monte,z) = high_correct(z)/(monte*20);
        disp('Center')
        disp(incl_ctrl_nr(z))
        disp(monte/realizations)
    end
end

%% corner
%incl_ctrl_nr=3; %number of controllers to include in calculation


high_correct=zeros(1,length(incl_ctrl_nr));


for z=1:length(incl_ctrl_nr)
    rssi = rssi_clean_corner-(n_corner.*loss)+noise;
    
    esti_room=zeros(1,size(rssi,1));
    ctrl_err=zeros(1,incl_ctrl_nr(z));
    err=zeros(1,incl_ctrl_nr(z));
    for monte =1:realizations
        temp = 0;
        for sensor_nr=1:size(rssi,1) %Test for sensor number
            [B,I]=maxk(rssi(sensor_nr,:,monte),incl_ctrl_nr(z)); %Find the controllers with the highest RSSI to the sensor
            for room_nr=1:length(B) %Calculate the error for a given room

                room_center=[(rooms(I(room_nr),1)+rooms(I(room_nr),3))/2 (rooms(I(room_nr),2)+rooms(I(room_nr),4))/2];

                for ctrl_nr=1:length(B) %test for the controllers
                    % find dist
                    % max
                    controller_room_corner_dist=[sqrt((rooms(I(room_nr),1)-beaconx_corner(I(ctrl_nr),monte))^2+(rooms(I(room_nr),2)-beacony_corner(I(ctrl_nr),monte))^2)
                                                 sqrt((rooms(I(room_nr),1)-beaconx_corner(I(ctrl_nr),monte))^2+(rooms(I(room_nr),4)-beacony_corner(I(ctrl_nr),monte))^2)
                                                 sqrt((rooms(I(room_nr),3)-beaconx_corner(I(ctrl_nr),monte))^2+(rooms(I(room_nr),2)-beacony_corner(I(ctrl_nr),monte))^2)
                                                 sqrt((rooms(I(room_nr),3)-beaconx_corner(I(ctrl_nr),monte))^2+(rooms(I(room_nr),4)-beacony_corner(I(ctrl_nr),monte))^2)];
                    controller_room_dist_max=max(controller_room_corner_dist);

                    % min
                    if beaconx_corner(I(ctrl_nr),monte) > rooms(I(room_nr),1) && beaconx_corner(I(ctrl_nr),monte) < rooms(I(room_nr),3) && beacony_corner(I(ctrl_nr),monte) > rooms(I(room_nr),2) && beacony_corner(I(ctrl_nr),monte) < rooms(I(room_nr),4)
                        controller_room_dist_min=0;
                    elseif beaconx_corner(I(ctrl_nr),monte) > rooms(I(room_nr),1) && beaconx_corner(I(ctrl_nr),monte) < rooms(I(room_nr),3)
                        ywalldist=[abs(beacony_corner(I(ctrl_nr),monte)-rooms(I(room_nr),2))
                                   abs(beacony_corner(I(ctrl_nr),monte)-rooms(I(room_nr),4))];
                        controller_room_dist_min=min(ywalldist);
                    elseif beacony_corner(I(ctrl_nr),monte) > rooms(I(room_nr),2) && beacony_corner(I(ctrl_nr),monte) < rooms(I(room_nr),2)
                        xwalldist=[abs(beaconx_corner(I(ctrl_nr),monte)-rooms(I(room_nr),1))
                                   abs(beaconx_corner(I(ctrl_nr),monte)-rooms(I(room_nr),3))];
                        controller_room_dist_min=min(xwalldist);       
                    else
                        controller_room_dist_min=min(controller_room_corner_dist);
                    end

                    % number of walls between center of room and controller
                    [XI, YI] = polyxpoly(wallx, wally, [room_center(1) beaconx_corner(I(ctrl_nr),monte)],[room_center(2) beacony_corner(I(ctrl_nr),monte)]);
                    n(ctrl_nr) = length(XI);
                    rssi_wall_est=B(ctrl_nr)+n(ctrl_nr)*loss; %subtract estimated wall loss from rssi
                    ctrl_sens_esti_dist = 0.5*exp(-0.1120479364*rssi_wall_est - 4.703918033); %Find estimated sensor controller distance

                    if ctrl_sens_esti_dist > controller_room_dist_max
                        ctrl_err(ctrl_nr)= ctrl_sens_esti_dist - controller_room_dist_max;
                    elseif ctrl_sens_esti_dist < controller_room_dist_min
                        ctrl_err(ctrl_nr)= controller_room_dist_min - ctrl_sens_esti_dist;
                    else
                        ctrl_err(ctrl_nr)=0;
                    end

                end
                err(room_nr) = sqrt(sum(ctrl_err.^2));
            end
            [M,In]=min(err);
            esti_room(sensor_nr)=I(In);

        end
        for i=1:size(esti_room,2)
            if esti_room(i)==i
                high_correct(z) = high_correct(z) + 1;
                temp = temp + 1;
            end
        end
        correct_corner(monte,z) = temp/20;
        ratio(monte,z) = high_correct(z)/(monte*20);
        disp('Corner')
        disp(incl_ctrl_nr(z))
        disp(monte/realizations)
    end
end


%% Plotting and etc.

figure(1)
hold on
%plot(pgon)
for i = 0:size(plan,1)-1
    p(i+1) = plot([plan(i+1,1) plan(i+1,3)],[plan(i+1,2) plan(i+1,4)],'k');
    p(i+1).LineWidth = plan(i+1,5);
end
%scatter(beaconx(:,end),beacony(:,end),'filled')
scatter(beaconx_center(:,end),beacony_center(:,end),'filled')
scatter(beaconx_corner(:,end),beacony_corner(:,end),'filled')
axis([-1 30 -1 30]);
axis equal

%Average room size clac below, for sanity
room_size = mean((rooms(:,3)-rooms(:,1)).*(rooms(:,4)-rooms(:,2)));

if realizations > 1
    figure(10)
    hold on
    plot(ratio)
    xlim([1 realizations])
    ylim([0 1])
    %ratio(end)
    legend('0 dB','2 dB','4 dB','6 dB','8 dB','10 dB','12 dB','15 dB')
    title('Accuracy ratio evolution over realizations')
    xlabel('Realizations')
    ylabel('Accuracy ratio')
end

z = 2.58; % equal to 95% interval
sigma = std(correct);
sigma_center = std(correct_center);
sigma_corner = std(correct_corner);

%calc lower 
lower = mean(correct)-z*(sigma/sqrt(realizations));
lower_center = mean(correct_center)-z*(sigma_center/sqrt(realizations));
lower_corner = mean(correct_corner)-z*(sigma_corner/sqrt(realizations));

%calc upper 
upper = mean(correct)+z*(sigma/sqrt(realizations));
upper_center = mean(correct_center)+z*(sigma_center/sqrt(realizations));
upper_corner = mean(correct_corner)+z*(sigma_corner/sqrt(realizations));

if realizations > 1
    figure(12)
    hold on
    %plot mean 
    plot(incl_ctrl_nr,mean(correct),'b-o')
    plot(incl_ctrl_nr,mean(correct_center),'r-o')
    plot(incl_ctrl_nr,mean(correct_corner),'m-o')
    %plot lower confidence interval
    plot(incl_ctrl_nr,lower(:),'b--')
    plot(incl_ctrl_nr,lower_center(:),'r--')
    plot(incl_ctrl_nr,lower_corner(:),'m--')
    %plot lower upper interval
    plot(incl_ctrl_nr,upper(:),'b--')
    plot(incl_ctrl_nr,upper_center(:),'r--')
    plot(incl_ctrl_nr,upper_corner(:),'m--')
    xline(2,'--');
    xlim([0 incl_ctrl_nr(end)])
    ylim([0 1])
    legend('Random','Center','Corner')
    title('Accuracy ratio over dB loss')
    xlabel('dB loss per wall')
    ylabel('Accuracy ratio')
    grid
end


if realizations > 1
    figure(13)
    hold on
    
    yy = [lower,fliplr(upper)];   % vector of upper & lower boundaries
    fill([incl_ctrl_nr,fliplr(incl_ctrl_nr)],yy,'b','edgealpha',0,'facealpha',.3) 
    
    yy = [lower_center,fliplr(upper_center)];   % vector of upper & lower boundaries
    fill([incl_ctrl_nr,fliplr(incl_ctrl_nr)],yy,'r','edgealpha',0,'facealpha',.3) 
    
    yy = [lower_corner,fliplr(upper_corner)];   % vector of upper & lower boundaries
    fill([incl_ctrl_nr,fliplr(incl_ctrl_nr)],yy,'m','edgealpha',0,'facealpha',.3) 
    
    %plot mean 
    plot(incl_ctrl_nr,mean(correct),'b-o')
    plot(incl_ctrl_nr,mean(correct_center),'r-o')
    plot(incl_ctrl_nr,mean(correct_corner),'m-o')
    xline(2,'--');
    xlim([2 incl_ctrl_nr(end)])
    ylim([0 1])
    legend('Random','Center','Corner')
    title('Accuracy ratio over number of rooms considered')
    xlabel('Number of rooms')
    ylabel('Accuracy ratio')
    grid
end