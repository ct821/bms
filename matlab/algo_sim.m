clc;
close all;
clear all;

%Simulation parameters
realizations = 100;
scale = 1;

plan = csvread("a3.csv");
plan = plan.*scale;

wall_loss=10; % How much is signal is lost through walls in dB

%Make walls from plan
wallx = zeros(size(plan,1)*3);
wally = zeros(size(plan,1)*3);
%make walls var
for i = 0:size(plan,1)-1
    wallx((i*3)+1) = plan(i+1,1);
    wallx((i*3)+2) = plan(i+1,3);
    wallx((i*3)+3) = NaN;
    wally((i*3)+1) = plan(i+1,2);
    wally((i*3)+2) = plan(i+1,4);
    wally((i*3)+3) = NaN;
end

%define areas in which points should be randomized
rooms = [   0 0 2.98 6.1            1;
    2.98 0 6.01 6.1         2;
    6.01 0 9.04 6.1         3;
    9.04 0 12.01 6.1        4;
    12.01 0 16.01 6.1       5;
    16.01 0 19.88 6.1       6;
    22.01 0 24.14 6.1       7;
    0 8.08 2.98 12.01       8;
    2.98 8.08 6.01 12.01    9;
    6.01 8.08 9.04 12.01    10;
    9.04 8.08 12.01 12.01   11;
    12.01 12.01 16.01 15.03 12;
    12.01 15.03 16.01 18.16 13;
    12.01 18.16 16.01 21.13 14;
    12.01 21.13 16.01 24.15 15;
    18.07 9 24.14 13.2      16;
    18.07 13.2 24.14 15.03  17;
    18.07 15.03 24.14 18.08 18;
    18.07 18.08 24.14 21.03 19;
    18.07 21.03 24.14 24.15 20];
rooms = rooms.*scale;

%Make and define rooms
for i = 1:size(rooms,1)
    pgon(i) = polyshape([rooms(i,1) rooms(i,1) rooms(i,3) rooms(i,3)],[rooms(i,4) rooms(i,2) rooms(i,2) rooms(i,4)]);
end


%Generate beacon(CU) and sensor pairs
beaconx = zeros(size(rooms,1),realizations);
beacony = zeros(size(rooms,1),realizations);
sensorx = zeros(size(rooms,1),realizations);
sensory = zeros(size(rooms,1),realizations);

%find distance between sensor and beacon
dist = zeros(size(rooms,1),size(rooms,1));
rssi_clean = zeros(size(rooms,1),size(rooms,1),realizations);
rssi_test = zeros(size(rooms,1),size(rooms,1),realizations);

%For every wanted realization in the monte carlo
for monte = 1:realizations
    hold on
    for i = 1:size(rooms,1)
        beaconx(i,monte) = rooms(i,1)+ (rooms(i,3)-rooms(i,1))*rand(1,1);
        beacony(i,monte) = rooms(i,2)+ (rooms(i,4)-rooms(i,2))*rand(1,1);
        
        sensorx(i,monte) = rooms(i,1)+ (rooms(i,3)-rooms(i,1))*rand(1,1);
        sensory(i,monte) = rooms(i,2)+ (rooms(i,4)-rooms(i,2))*rand(1,1);
    end
    
    %find distance between sensor and beacon
    for i = 1:size(rooms,1)
        for j = 1:size(rooms,1)
            dist(i,j) = sqrt((beaconx(j,monte)-sensorx(i,monte)).^2+(beacony(j,monte)-sensory(i,monte)).^2);
            
            [XI, YI] = polyxpoly(wallx, wally, [sensorx(i,monte) beaconx(j,monte)],[sensory(i,monte) beacony(j,monte)]);
            n = length(XI);
            
            %lige her er den grande formel!!!!!!!!!!!!
            %rssi(i,j) = n;%
            rssi_clean(i,j,monte)=((-41.9813) + (10*(-2.055)*log10(dist(i,j)/0.5))) -(n*wall_loss);
            rssi_test(i,j,monte)=((-41.9813) + (10*(-2.055)*log10(dist(i,j)/0.5)));
        end
    end
end

%Add noise to the RSSI measurements
noise = randn(size(rooms,1),size(rooms,1),realizations).*sqrt(22.159);
rssi=rssi_clean+noise;

ratio = zeros(realizations,7);

%% Highest RSSI method
figure(1)
hold on
high_correct = 0;

%Highest RSSI method, check if each sensor would be paired correctly
for monte = 1:realizations
    for i = 1:size(rssi,1)
        [M, I] = max(rssi(i,:,monte));
        
        if monte == realizations
            %Plot connections
            plot([sensorx(i,monte) beaconx(I,monte)],[sensory(i,monte) beacony(I,monte)], "--")
        end
        if I==i
            high_correct=high_correct+1;
        end
    end
    
    %How large a procentage is correct so far
    ratio(monte,1) = high_correct/(monte*20)
end

%Just ploting stuff below here
%Debug rooms
plot(pgon)

%Plot walls
for i = 0:size(plan,1)-1
    p(i+1) = plot([plan(i+1,1) plan(i+1,3)],[plan(i+1,2) plan(i+1,4)],'k');
    p(i+1).LineWidth = plan(i+1,5);
end

%Add CU and sensors
scatter(beaconx(:,end),beacony(:,end))
scatter(sensorx(:,end),sensory(:,end), 'x')

%Plot axis
axis([-1 30 -1 30]);
axis equal
title('Highest RSSI')
hold off


%% Least squares method
figure(2)
hold on

LS_correct = 0;

esti_dist = 0.5*exp(-0.1120479364*rssi_test - 4.703918033);

trilat = zeros(size(rssi,1),2,monte);
connection = zeros(size(rssi,1),monte);

for monte = 1:realizations
    for i = 1:size(rssi,1)
        [B,I] = maxk(rssi(i,:,monte),3);
        
        x1=beaconx(I(1));
        y1=beacony(I(1));
        r1=(esti_dist(i,I(1)));
        x2=beaconx(I(2));
        y2=beacony(I(2));
        r2=(esti_dist(i,I(2)));
        x3=beaconx(I(3));
        y3=beacony(I(3));
        r3=(esti_dist(i,I(3)));
        
        A=2*[x2 - x1, y2 - y1; %A matrix når x1,y1 ikke er sat i 0,0
            x3 - x1, y3 - y1];
        
        b= [x2^2 + y2^2 + r1^2 - x1^2 - y1^2 - r2^2; %B Matrix når x1,y1 ikke er sat i 0,0
            x3^2 + y3^2 + r1^2 - x1^2 - y1^2 - r3^2];
        
        trilat(i,:,monte) = (A'*A)\A'*b; %Least squares definition
        
        if monte == realizations
            scatter(trilat(i,1,monte),trilat(i,2,monte),'o','filled')
        end
        
        %viscircles([x1 y1],r1, 'Linewidth',1);
        %viscircles([x2 y2],r2, 'Linewidth',1);
        %viscircles([x3 y3],r3, 'Linewidth',1);
        
    end
    
    for j = 1:size(trilat,1)
        for i = 1:size(rssi,1)
            if trilat(j,1,monte)<=rooms(i,3) && trilat(j,1,monte)>=rooms(i,1) && trilat(j,2,monte)<=rooms(i,4) && trilat(j,2,monte)>=rooms(i,2)
                if i == j
                    LS_correct=LS_correct+1;
                end
                connection(j,monte) = i;
                if monte == realizations
                    plot([sensorx(j,monte) beaconx(i,monte)],[sensory(j,monte) beacony(i,monte)], "--")
                end
            end
        end
    end
    ratio(monte,2) = LS_correct/(monte*20)
end

plot(pgon)

%Plot walls
for i = 0:size(plan,1)-1
    p(i+1) = plot([plan(i+1,1) plan(i+1,3)],[plan(i+1,2) plan(i+1,4)],'k');
    p(i+1).LineWidth = plan(i+1,5);
end

%Add CU and sensors
scatter(beaconx(:,end),beacony(:,end))
scatter(sensorx(:,end),sensory(:,end), 'x')

%Plot axis
axis([-1 30 -1 30]);
axis equal
title('Least squares')
hold off

%% Least squares with outside room correction
figure(3)
hold on

LS2_correct = 0;

room_dist = zeros(size(rssi,1),1);

x_center = ((rooms(:,3)-rooms(:,1))/2)+rooms(:,1);
y_center = ((rooms(:,4)-rooms(:,2))/2)+rooms(:,2);

connection2 = connection;

for monte = 1:realizations
    for j = 1:size(trilat,1)
        if connection2(j,monte) == 0
            for i = 1:size(rssi,1)
                room_dist(i) = sqrt((trilat(j,1,monte)-x_center(i))^2+(trilat(j,2,monte)-y_center(i))^2);
            end
            [M,I] = min(room_dist);
            connection2(j,monte) = I;
        end
    end
    
    for i = 1:size(rssi,1)
        if connection2(i,monte) == i
            LS2_correct=LS2_correct+1;
        end
        if monte == realizations
            plot([sensorx(i,monte) beaconx(connection2(i,monte),monte)],[sensory(i,monte) beacony(connection2(i,monte),monte)], "--")
            plot([trilat(i,1,monte) beaconx(connection2(i,monte),monte)],[trilat(i,2,monte) beacony(connection2(i,monte),monte)], "-")
            scatter(trilat(i,1,monte),trilat(i,2,monte),'o','filled')
        end
    end
    ratio(monte,3) = LS2_correct/(monte*20)
end

plot(pgon)

%Plot walls
for i = 0:size(plan,1)-1
    p(i+1) = plot([plan(i+1,1) plan(i+1,3)],[plan(i+1,2) plan(i+1,4)],'k');
    p(i+1).LineWidth = plan(i+1,5);
end

%Add CU and sensors
scatter(beaconx(:,end),beacony(:,end))
scatter(sensorx(:,end),sensory(:,end), 'x')

%Plot axis
axis([-1 30 -1 30]);
axis equal
title('Least squares with room correction')
hold off

%% Non linear solution

figure(4)
hold on

opti_correct = 0;

connection3 = zeros(size(rssi,1),monte);
estimates = zeros(size(rssi,1),2,monte);

esti_dist = 0.5*exp(-0.1120479364*rssi - 4.703918033);

%estimate point
res = 0.1*scale;
x = -10:res:35*scale;
y = -10:res:35*scale;

err1 = zeros(length(x),length(y));
err2 = zeros(length(x),length(y));
err3 = zeros(length(x),length(y));
err = zeros(length(x),length(y));

for monte = 1:realizations
    for i = 1:size(rssi,1)
        [B,I] = maxk(rssi(i,:,monte),3);
        
        for j = 1:length(x)
            for k = 1:length(y)
                err1(k,j) = abs(sqrt((beaconx(I(1),monte)-x(j))^2+(beacony(I(1),monte)-y(k))^2)-esti_dist(i,I(1),monte));
                err2(k,j) = abs(sqrt((beaconx(I(2),monte)-x(j))^2+(beacony(I(2),monte)-y(k))^2)-esti_dist(i,I(2),monte));
                err3(k,j) = abs(sqrt((beaconx(I(3),monte)-x(j))^2+(beacony(I(3),monte)-y(k))^2)-esti_dist(i,I(3),monte));
                
                err(k,j) = err1(k,j)^2+err2(k,j)^2+err3(k,j)^2;
            end
        end
        minMatrix = min(err(:));
        [row,col] = find(err==minMatrix);
        estimates(i,1:2,monte) = [x(col),y(row)];
        
        if i == 1 && monte == realizations
            viscircles([beaconx(I(1),monte) beacony(I(1),monte)],esti_dist(i,I(1),monte), 'Linewidth',1);
            viscircles([beaconx(I(2),monte) beacony(I(2),monte)],esti_dist(i,I(2),monte), 'Linewidth',1);
            viscircles([beaconx(I(3),monte) beacony(I(3),monte)],esti_dist(i,I(3),monte), 'Linewidth',1);
            scatter(estimates(i,1,monte),estimates(i,2,monte),'o','filled')
            %estimates(i,:,monte)
        end
        
    end
    
    for j = 1:size(estimates,1)
        for i = 1:size(rssi,1)
            if estimates(j,1,monte)<=rooms(i,3) && estimates(j,1,monte)>=rooms(i,1) && estimates(j,2,monte)<=rooms(i,4) && estimates(j,2,monte)>=rooms(i,2)
                if i == j
                    opti_correct=opti_correct+1;
                end
                connection3(j,monte) = i;
                if monte == realizations
                    plot([sensorx(j,monte) beaconx(i,monte)],[sensory(j,monte) beacony(i,monte)], "--")
                    %scatter(estimates(i,1,monte),estimates(i,2,monte),'o','filled')
                end
            end
        end
    end
    
    ratio(monte,4) = opti_correct/(monte*20)
end

plot(pgon)

%Plot walls
for i = 0:size(plan,1)-1
    p(i+1) = plot([plan(i+1,1) plan(i+1,3)],[plan(i+1,2) plan(i+1,4)],'k');
    p(i+1).LineWidth = plan(i+1,5);
end

%Add CU and sensors
scatter(beaconx(:,end),beacony(:,end))
scatter(sensorx(:,end),sensory(:,end), 'x')

%Plot axis
axis([-1 30 -1 30]);
axis equal
title('Surface minima')
hold off

%% non linear with room correction

figure(5)
hold on

opti_correct2 = 0;

room_dist = zeros(size(rssi,1),1);
x_center = ((rooms(:,3)-rooms(:,1))/2)+rooms(:,1);
y_center = ((rooms(:,4)-rooms(:,2))/2)+rooms(:,2);

connection4 = connection3;

for monte = 1:realizations
    for j = 1:size(trilat,1)
        if connection4(j,monte) == 0
            for i = 1:size(rssi,1)
                room_dist(i) = sqrt((estimates(j,1,monte)-x_center(i))^2+(estimates(j,2,monte)-y_center(i))^2);
            end
            [M,I] = min(room_dist);
            connection4(j,monte) = I;
        end
    end
    
    for i = 1:size(rssi,1)
        if connection4(i,monte) == i
            opti_correct2=opti_correct2+1;
        end
        if monte == realizations
            plot([sensorx(i,monte) beaconx(connection4(i,monte),monte)],[sensory(i,monte) beacony(connection4(i,monte),monte)], "--")
            plot([estimates(i,1,monte) beaconx(connection4(i,monte),monte)],[estimates(i,2,monte) beacony(connection4(i,monte),monte)], "-")
            scatter(estimates(i,1,monte),estimates(i,2,monte),'o','filled')
        end
    end
    ratio(monte,5) = opti_correct2/(monte*20)
end

plot(pgon)

%Plot walls
for i = 0:size(plan,1)-1
    p(i+1) = plot([plan(i+1,1) plan(i+1,3)],[plan(i+1,2) plan(i+1,4)],'k');
    p(i+1).LineWidth = plan(i+1,5);
end

%Add CU and sensors
scatter(beaconx(:,end),beacony(:,end))
scatter(sensorx(:,end),sensory(:,end), 'x')

%Plot axis
axis([-1 30 -1 30]);
axis equal
title('Surface minima with room correction')
hold off


%% TEST part 1

figure(6)
hold on

opti_correct = 0;

connection3 = zeros(size(rssi,1),monte);
estimates = zeros(size(rssi,1),2,monte);

esti_dist = 0.5*exp(-0.1120479364*rssi_test - 4.703918033);

%estimate point
res = 0.1*scale;
x = -10:res:35*scale;
y = -10:res:35*scale;

err1 = zeros(length(x),length(y));
err2 = zeros(length(x),length(y));
err3 = zeros(length(x),length(y));
err = zeros(length(x),length(y));

for monte = 1:realizations
    for i = 1:size(rssi,1)
        [B,I] = maxk(rssi(i,:,monte),3);
        
        for j = 1:length(x)
            for k = 1:length(y)
                err1(k,j) = abs(sqrt((beaconx(I(1),monte)-x(j))^2+(beacony(I(1),monte)-y(k))^2)-esti_dist(i,I(1),monte));
                err2(k,j) = abs(sqrt((beaconx(I(2),monte)-x(j))^2+(beacony(I(2),monte)-y(k))^2)-esti_dist(i,I(2),monte));
                err3(k,j) = abs(sqrt((beaconx(I(3),monte)-x(j))^2+(beacony(I(3),monte)-y(k))^2)-esti_dist(i,I(3),monte));
                
                err(k,j) = err1(k,j)^2+err2(k,j)^2+err3(k,j)^2;
            end
        end
        minMatrix = min(err(:));
        [row,col] = find(err==minMatrix);
        estimates(i,1:2,monte) = [x(col),y(row)];
        
        if i == 1 && monte == realizations
            viscircles([beaconx(I(1),monte) beacony(I(1),monte)],esti_dist(i,I(1),monte), 'Linewidth',1);
            viscircles([beaconx(I(2),monte) beacony(I(2),monte)],esti_dist(i,I(2),monte), 'Linewidth',1);
            viscircles([beaconx(I(3),monte) beacony(I(3),monte)],esti_dist(i,I(3),monte), 'Linewidth',1);
            scatter(estimates(i,1,monte),estimates(i,2,monte),'o','filled')
            %estimates(i,:,monte)
        end
        
    end
    
    for j = 1:size(estimates,1)
        for i = 1:size(rssi,1)
            if estimates(j,1,monte)<=rooms(i,3) && estimates(j,1,monte)>=rooms(i,1) && estimates(j,2,monte)<=rooms(i,4) && estimates(j,2,monte)>=rooms(i,2)
                if i == j
                    opti_correct=opti_correct+1;
                end
                connection3(j,monte) = i;
                if monte == realizations
                    plot([sensorx(j,monte) beaconx(i,monte)],[sensory(j,monte) beacony(i,monte)], "--")
                    %scatter(estimates(i,1,monte),estimates(i,2,monte),'o','filled')
                end
            end
        end
    end
    
    ratio(monte,6) = opti_correct/(monte*20)
end

plot(pgon)

%Plot walls
for i = 0:size(plan,1)-1
    p(i+1) = plot([plan(i+1,1) plan(i+1,3)],[plan(i+1,2) plan(i+1,4)],'k');
    p(i+1).LineWidth = plan(i+1,5);
end

%Add CU and sensors
scatter(beaconx(:,end),beacony(:,end))
scatter(sensorx(:,end),sensory(:,end), 'x')

%Plot axis
axis([-1 30 -1 30]);
axis equal
title('Surface minima')
hold off

%% TEST part 2

figure(7)
hold on

opti_correct2 = 0;

room_dist = zeros(size(rssi,1),1);
x_center = ((rooms(:,3)-rooms(:,1))/2)+rooms(:,1);
y_center = ((rooms(:,4)-rooms(:,2))/2)+rooms(:,2);

connection4 = connection3;

for monte = 1:realizations
    for j = 1:size(trilat,1)
        if connection4(j,monte) == 0
            for i = 1:size(rssi,1)
                room_dist(i) = sqrt((estimates(j,1,monte)-x_center(i))^2+(estimates(j,2,monte)-y_center(i))^2);
            end
            [M,I] = min(room_dist);
            connection4(j,monte) = I;
        end
    end
    
    for i = 1:size(rssi,1)
        if connection4(i,monte) == i
            opti_correct2=opti_correct2+1;
        end
        if monte == realizations
            plot([sensorx(i,monte) beaconx(connection4(i,monte),monte)],[sensory(i,monte) beacony(connection4(i,monte),monte)], "--")
            plot([estimates(i,1,monte) beaconx(connection4(i,monte),monte)],[estimates(i,2,monte) beacony(connection4(i,monte),monte)], "-")
            scatter(estimates(i,1,monte),estimates(i,2,monte),'o','filled')
        end
    end
    ratio(monte,7) = opti_correct2/(monte*20)
end

plot(pgon)

%Plot walls
for i = 0:size(plan,1)-1
    p(i+1) = plot([plan(i+1,1) plan(i+1,3)],[plan(i+1,2) plan(i+1,4)],'k');
    p(i+1).LineWidth = plan(i+1,5);
end

%Add CU and sensors
scatter(beaconx(:,end),beacony(:,end))
scatter(sensorx(:,end),sensory(:,end), 'x')

%Plot axis
axis([-1 30 -1 30]);
axis equal
title('Surface minima with room correction')
hold off


%% Squared distance error
incl_ctrl_nr=3;
correct=0;
esti_room=zeros(1,size(rssi,1));
ctrl_err=zeros(1,incl_ctrl_nr);
err=zeros(1,incl_ctrl_nr);
for monte =1:realizations
    for sensor_nr=1:size(rssi,1) %Test for sensor number
        [B,I]=maxk(rssi(sensor_nr,:,monte),incl_ctrl_nr); %Find the controllers with the highest RSSI to the sensor
        for room_nr=1:length(B) %Calculate the error for a given room
            
            room_center=[(rooms(I(room_nr),1)+rooms(I(room_nr),3))/2 (rooms(I(room_nr),2)+rooms(I(room_nr),4))/2];

            for ctrl_nr=1:length(B) %test for the controllers
                % find dist
                % max
                controller_room_corner_dist=[sqrt((rooms(I(room_nr),1)-beaconx(I(ctrl_nr),monte))^2+(rooms(I(room_nr),2)-beacony(I(ctrl_nr),monte))^2)
                                             sqrt((rooms(I(room_nr),1)-beaconx(I(ctrl_nr),monte))^2+(rooms(I(room_nr),4)-beacony(I(ctrl_nr),monte))^2)
                                             sqrt((rooms(I(room_nr),3)-beaconx(I(ctrl_nr),monte))^2+(rooms(I(room_nr),2)-beacony(I(ctrl_nr),monte))^2)
                                             sqrt((rooms(I(room_nr),3)-beaconx(I(ctrl_nr),monte))^2+(rooms(I(room_nr),4)-beacony(I(ctrl_nr),monte))^2)];
                controller_room_dist_max=max(controller_room_corner_dist);

                % min
                if beaconx(I(ctrl_nr),monte) > rooms(I(room_nr),1) && beaconx(I(ctrl_nr),monte) < rooms(I(room_nr),3) && beacony(I(ctrl_nr),monte) > rooms(I(room_nr),2) && beacony(I(ctrl_nr),monte) < rooms(I(room_nr),4)
                    controller_room_dist_min=0;
                elseif beaconx(I(ctrl_nr),monte) > rooms(I(room_nr),1) && beaconx(I(ctrl_nr),monte) < rooms(I(room_nr),3)
                    ywalldist=[abs(beacony(I(ctrl_nr),monte)-rooms(I(room_nr),2))
                               abs(beacony(I(ctrl_nr),monte)-rooms(I(room_nr),4))];
                    controller_room_dist_min=min(ywalldist);
                elseif beacony(I(ctrl_nr),monte) > rooms(I(room_nr),2) && beacony(I(ctrl_nr),monte) < rooms(I(room_nr),2)
                    xwalldist=[abs(beaconx(I(ctrl_nr),monte)-rooms(I(room_nr),1))
                               abs(beaconx(I(ctrl_nr),monte)-rooms(I(room_nr),3))];
                    controller_room_dist_min=min(xwalldist);       
                else
                    controller_room_dist_min=min(controller_room_corner_dist);
                end

                % number of walls between center of room and controller
                [XI, YI] = polyxpoly(wallx, wally, [room_center(1) beaconx(I(ctrl_nr),monte)],[room_center(2) beacony(I(ctrl_nr),monte)]);
                n(ctrl_nr) = length(XI);
                rssi_wall_est=B(ctrl_nr)+n(ctrl_nr)*wall_loss; %subtract estimated wall loss from rssi
                ctrl_sens_esti_dist = 0.5*exp(-0.1120479364*rssi_wall_est - 4.703918033); %Find estimated sensor controller distance
                
                if ctrl_sens_esti_dist > controller_room_dist_max
                    ctrl_err(ctrl_nr)= ctrl_sens_esti_dist - controller_room_dist_max;
                elseif ctrl_sens_esti_dist < controller_room_dist_min
                    ctrl_err(ctrl_nr)= controller_room_dist_min - ctrl_sens_esti_dist;
                else
                    ctrl_err(ctrl_nr)=0;
                end
                
            end
            err(room_nr) = sqrt(sum(ctrl_err.^2));
        end
        [M,In]=min(err);
        esti_room(sensor_nr)=I(In);
        
    end
    for i=1:size(esti_room,2)
        if esti_room(i)==i
            correct = correct + 1;
        end
    end
    ratio(monte,8) = correct/(monte*20);
end

%% Plotting and etc.

%Average room size clac below, for sanity
room_size = mean((rooms(:,3)-rooms(:,1)).*(rooms(:,4)-rooms(:,2)));

if realizations > 1
    figure()
    plot(ratio)
    xlim([1 realizations])
    ylim([0 1])
    %ratio(end)
    legend('Highest RSSI','Least squares','Least squares with correction','Surface minima','Surface minima with correction','No walls','No walls with correction')
    title('Accuracy ratio evolution over realizations')
    xlabel('Realizations')
    ylabel('Accuracy ratio')
end

