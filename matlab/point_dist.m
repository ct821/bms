function dist = point_dist(X1,Y1,X2,Y2)
dist=sqrt((X2-X1)^2+(Y2-Y1)^2);
end

