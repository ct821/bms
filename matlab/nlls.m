clc;
close all;
clear all;
tic
%Simulation parameters
realizations = 3;
scales_i = 1;
scales = linspace(1,10,scales_i);

plan = csvread("a3.csv");

%Make walls from plan
wallx = zeros(size(plan,1)*3);
wally = zeros(size(plan,1)*3);
%make walls var
for i = 0:size(plan,1)-1
    wallx((i*3)+1) = plan(i+1,1);
    wallx((i*3)+2) = plan(i+1,3);
    wallx((i*3)+3) = NaN;
    wally((i*3)+1) = plan(i+1,2);
    wally((i*3)+2) = plan(i+1,4);
    wally((i*3)+3) = NaN;
end

%define areas in which points should be randomized
for i = 1:scales_i
    rooms(:,:,i) = [0     0     2.98  6.1   1;
                    2.98  0     6.01  6.1   2;
                    6.01  0     9.04  6.1   3;
                    9.04  0     12.01 6.1   4;
                    12.01 0     16.01 6.1   5;
                    16.01 0     19.88 6.1   6;
                    22.01 0     24.14 6.1   7;
                    0     8.08  2.98  12.01 8;
                    2.98  8.08  6.01  12.01 9;
                    6.01  8.08  9.04  12.01 10;
                    9.04  8.08  12.01 12.01 11;
                    12.01 12.01 16.01 15.03 12;
                    12.01 15.03 16.01 18.16 13;
                    12.01 18.16 16.01 21.13 14;
                    12.01 21.13 16.01 24.15 15;
                    18.07 9     24.14 13.2  16;
                    18.07 13.2  24.14 15.03 17;
                    18.07 15.03 24.14 18.08 18;
                    18.07 18.08 24.14 21.03 19;
                    18.07 21.03 24.14 24.15 20];
%    rooms(:,:,i) = rooms(:,:,i).*scales(i);
end

%Make and define rooms
for i = 1:size(rooms,1)
    pgon(i) = polyshape([rooms(i,1) rooms(i,1) rooms(i,3) rooms(i,3)],[rooms(i,4) rooms(i,2) rooms(i,2) rooms(i,4)]);
end


%Generate beacon(CU) and sensor pairs
beaconx = zeros(size(rooms,1),realizations,scales_i);
beacony = zeros(size(rooms,1),realizations,scales_i);
sensorx = zeros(size(rooms,1),realizations,scales_i);
sensory = zeros(size(rooms,1),realizations,scales_i);

%Additional static beacon positions
beaconx_center = zeros(size(rooms,1),realizations);
beacony_center = zeros(size(rooms,1),realizations);
beaconx_corner = zeros(size(rooms,1),realizations);
beacony_corner = zeros(size(rooms,1),realizations);
for monte = 1:realizations
    beaconx_center(:,monte) = ((rooms(:,3)-rooms(:,1))/2)+rooms(:,1);
    beacony_center(:,monte) = ((rooms(:,4)-rooms(:,2))/2)+rooms(:,2);
end
offset = 0.3;
corner_x = [2.9800-offset
            6.0100-offset
            9.0400-offset
           12.0100-offset
           16.0100-offset
           19.8800-offset
           24.1400-offset
            0+offset
            2.9800+offset
            6.0100+offset
            9.0400+offset
           16.0100-offset
           16.0100-offset
           16.0100-offset
           16.0100-offset
           18.0700+offset
           18.0700+offset
           18.0700+offset
           18.0700+offset
           18.0700+offset];
       
corner_y = [ 6.1000-offset
            6.1000-offset
            6.1000-offset
            6.1000-offset
            6.1000-offset
            6.1000-offset
            6.1000-offset
            8.0800+offset
            8.0800+offset
            8.0800+offset
            8.0800+offset
           12.0100+offset
           15.0300+offset
           18.1600+offset
           21.1300+offset
           13.2000-offset
           15.0300-offset
           18.0800-offset
           21.0300-offset
           24.1500-offset];
       
for monte = 1:realizations
    beaconx_corner(:,monte) = corner_x;
    beacony_corner(:,monte) = corner_y;
end

%find distance between sensor and beacon
dist_random = zeros(size(rooms,1),size(rooms,1));
dist_center = zeros(size(rooms,1),size(rooms,1));
dist_corner = zeros(size(rooms,1),size(rooms,1));
rssi_clean_random = zeros(size(rooms,1),size(rooms,1),realizations);
rssi_clean_center = zeros(size(rooms,1),size(rooms,1),realizations);
rssi_clean_corner = zeros(size(rooms,1),size(rooms,1),realizations);
rssi_test = zeros(size(rooms,1),size(rooms,1),realizations);
n_random = zeros(size(rooms,1),size(rooms,1),realizations);
n_center = zeros(size(rooms,1),size(rooms,1),realizations);
n_corner = zeros(size(rooms,1),size(rooms,1),realizations);

%For every wanted realization in the monte carlo
for monte = 1:realizations
    hold on
    for i = 1:size(rooms,1)
        for j = 1:scales_i
            beaconx(i,monte,j) = rooms(i,1,j)+ (rooms(i,3,j)-rooms(i,1,j))*rand(1,1);
            beacony(i,monte,j) = rooms(i,2,j)+ (rooms(i,4,j)-rooms(i,2,j))*rand(1,1);
        
            sensorx(i,monte,j) = rooms(i,1,j)+ (rooms(i,3,j)-rooms(i,1,j))*rand(1,1);
            sensory(i,monte,j) = rooms(i,2,j)+ (rooms(i,4,j)-rooms(i,2,j))*rand(1,1);
        end
    end
    
    %find distance between sensor and beacon
    for i = 1:size(rooms,1)
        for j = 1:size(rooms,1)
            for k = 1:scales_i
                dist_random(i,j,k) = sqrt((beaconx(j,monte,k)-sensorx(i,monte,k)).^2+(beacony(j,monte,k)-sensory(i,monte,k)).^2);
                dist_center(i,j) = sqrt((beaconx_center(j,monte)-sensorx(i,monte)).^2+(beacony_center(j,monte)-sensory(i,monte)).^2);
                dist_corner(i,j) = sqrt((beaconx_corner(j,monte)-sensorx(i,monte)).^2+(beacony_corner(j,monte)-sensory(i,monte)).^2);
            
                [XI_random, YI_random] = polyxpoly(wallx, wally, [sensorx(i,monte,k) beaconx(j,monte,k)],[sensory(i,monte,k) beacony(j,monte,k)]);
                n_random(i,j,monte,k) = length(XI_random);
            
                [XI_center, YI_center] = polyxpoly(wallx, wally, [sensorx(i,monte) beaconx_center(j,monte)],[sensory(i,monte) beacony_center(j,monte)]);
                n_center(i,j,monte) = length(XI_center);
            
                [XI_corner, YI_corner] = polyxpoly(wallx, wally, [sensorx(i,monte) beaconx_corner(j,monte)],[sensory(i,monte) beacony_corner(j,monte)]);
                n_corner(i,j,monte) = length(XI_corner);
            
            %lige her er den grande formel!!!!!!!!!!!!
            %rssi(i,j) = n;%
                rssi_clean_random(i,j,monte,k)=((-41.9813) + (10*(-2.055)*log10(dist_random(i,j,k)/0.5)));
                rssi_clean_center(i,j,monte)=((-41.9813) + (10*(-2.055)*log10(dist_center(i,j)/0.5)));
                rssi_clean_corner(i,j,monte)=((-41.9813) + (10*(-2.055)*log10(dist_corner(i,j)/0.5)));
            end        
        end
    end
    monte/realizations
end

%Add noise to the RSSI measurements
%noise_levels = linspace(0,22.159*2,11)';

%for i = 1:length(noise_levels)
%    noise(:,:,:,i) = randn(size(rooms,1),size(rooms,1),realizations).*sqrt(noise_levels(i));
%end

noise_levels = 1;
noise = randn(size(rooms,1),size(rooms,1),realizations).*sqrt(22.159);

loss = [0;2;4;6;7;10;15];
%loss = 2;

ratio = zeros(realizations,length(loss));
correct_random = zeros(realizations,length(loss));
correct_center = zeros(realizations,length(loss));
correct_corner = zeros(realizations,length(loss));
correct_random_rc = zeros(realizations,length(loss));
correct_center_rc = zeros(realizations,length(loss));
correct_corner_rc = zeros(realizations,length(loss));

connection1 = zeros(size(rssi_clean_random,1),monte,max([length(noise_levels),length(loss)]));
connection3 = zeros(size(rssi_clean_center,1),monte,max([length(noise_levels),length(loss)]));
connection5 = zeros(size(rssi_clean_corner,1),monte,max([length(noise_levels),length(loss)]));

DIST_METHOD = 2;     % 1 = Old way, 2 = Alternative way
LATERATION  = 3;    % Max unit for lateration
LS          = 3;     % Min unit for lateration
MAX_ITE     = 100;   % Tweak
TOLERANCE   = 0.001; % Tweak

for i = LS:LATERATION
    LV(i-(LS-1),:) = i;
end

toc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Non-Linear Least Square - Random                                              1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
warning('off', 'MATLAB:nearlySingularMatrix');
tic
clear R
if length(noise_levels) ~= 1 && length(loss) == 1
    high_correct = zeros(1,length(noise_levels));
elseif length(noise_levels) == 1 && length(loss) ~= 1
    high_correct = zeros(1,length(loss));
elseif length(noise_levels) == 1 && length(loss) == 1
high_correct = zeros(1,scales_i);
%    high_correct = zeros(1,LATERATION-(LS-1));
end

for zz = 1:length(loss)
    for z = 1:length(noise_levels)
        rssi = rssi_clean_random-(n_random.*loss(zz))+noise(:,:,:,z);
        
        if DIST_METHOD == 1
            % Estimated distance
            esti_dist = 0.5*exp(-0.1120479364*rssi - 4.703918033);
        elseif DIST_METHOD == 2
            % Alternative way of estimating distance
            for ii = 1:scales_i
                wall =([0.3426/ii 0.3314/ii 0.3300/ii 0.3315/ii 0.2957/ii 0.2932/ii 0.3035/ii 0.2989/ii 0.3178/ii 0.3351/ii 0.3541/ii 0.3276/ii 0.3350/ii 0.3174/ii 0.2878/ii 0.2914/ii 0.3378/ii 0.3346/ii 0.3359/ii 0.3397/ii]); % average loss for individual rooms
                for i =1:length(wall)
                    esti_dist(i,:,:,ii) =0.5/(exp(lambertw(0.05602396820*wall(i)*loss(zz)*exp(-0.1120479364*rssi(i,:,:,ii) - 4.703772370)) + 0.1120479364*rssi(i,:,:,ii) + 4.703772370));
                end
            end
        end
        
        for monte = 1: realizations
            for sca = 1:scales_i
                for tri = LS:LATERATION
                    clear f Jf
                    temp = 0;
                    for i = 1:size(rssi,1)
                        [B,I] = maxk(rssi(i,:,monte,sca),tri);
                        
                        % Initial guesses
                        R{i,monte,1,z,zz,tri-(LS-1),sca}(1,1:2) = [0 0];
                        R{i,monte,2,z,zz,tri-(LS-1),sca}(1,1:2) = [30 30];
                        R{i,monte,3,z,zz,tri-(LS-1),sca}(1,1:2) = [0 30];
                        R{i,monte,4,z,zz,tri-(LS-1),sca}(1,1:2) = [30 0];
                        for j = 1:4
                            ITE = 1;
                            
                            % Functions
                            for ik = 1:tri
                                f(ik,:) = ( sqrt( (R{i,monte,j,z,zz,tri-(LS-1),sca}(1,1)-beaconx(I(ik),monte,sca))^2 + (R{i,monte,j,z,zz,tri-(LS-1),sca}(1,2)-beacony(I(ik),monte,sca))^2 ) - esti_dist(i,I(ik),monte,sca) )^2;
                            end
                            % Jacobian
                            for ik = 1:tri
                                Jf(ik,:) = [((2*R{i,monte,j,z,zz,tri-(LS-1),sca}(1,1)-2*beaconx(I(ik),monte,sca))*(((R{i,monte,j,z,zz,tri-(LS-1),sca}(1,1)-beaconx(I(ik),monte,sca))^2+(R{i,monte,j,z,zz,tri-(LS-1),sca}(1,2)-beacony(I(ik),monte,sca))^2)^(1/2)-esti_dist(i,I(ik),monte,sca)))/((R{i,monte,j,z,zz,tri-(LS-1),sca}(1,1)-beaconx(I(ik),monte,sca))^2+(R{i,monte,j,z,zz,tri-(LS-1),sca}(1,2)-beacony(I(ik),monte,sca))^2)^(1/2)   ((2*R{i,monte,j,z,zz,tri-(LS-1),sca}(1,2)-2*beacony(I(ik),monte,sca))*(((R{i,monte,j,z,zz,tri-(LS-1),sca}(1,1)-beaconx(I(ik),monte,sca))^2+(R{i,monte,j,z,zz,tri-(LS-1),sca}(1,2)-beacony(I(ik),monte,sca))^2)^(1/2)-esti_dist(i,I(ik),monte,sca)))/((R{i,monte,j,z,zz,tri-(LS-1),sca}(1,1)-beaconx(I(ik),monte,sca))^2+(R{i,monte,j,z,zz,tri-(LS-1),sca}(1,2)-beacony(I(ik),monte,sca))^2)^(1/2)];
                            end
                            
                            % Check if initial guess is sufficient
                            R{i,monte,j,z,zz,tri-(LS-1),sca}(1, 3) = norm(Jf' * f, 2);
                            if norm(Jf' * f, 2) < TOLERANCE
                                break
                            end
                            
                            % Gauss-Newton iteration, as long as we haven't hit the
                            % maximumm # of iterations and the norm > tolerance
                            while ITE < MAX_ITE %&& norm(Jf' * f, 2) > TOLERANCE
                                for ik = 1:tri
                                    f(ik,:) = ( sqrt( (R{i,monte,j,z,zz,tri-(LS-1),sca}(ITE,1)-beaconx(I(ik),monte,sca))^2 + (R{i,monte,j,z,zz,tri-(LS-1),sca}(ITE,2)-beacony(I(ik),monte,sca))^2 ) - esti_dist(i,I(ik),monte,sca) )^2;
                                end
                                for ik = 1:tri
                                    Jf(ik,:) = [((2*R{i,monte,j,z,zz,tri-(LS-1),sca}(ITE,1)-2*beaconx(I(ik),monte,sca))*(((R{i,monte,j,z,zz,tri-(LS-1),sca}(ITE,1)-beaconx(I(ik),monte,sca))^2+(R{i,monte,j,z,zz,tri-(LS-1),sca}(ITE,2)-beacony(I(ik),monte,sca))^2)^(1/2)-esti_dist(i,I(ik),monte,sca)))/((R{i,monte,j,z,zz,tri-(LS-1),sca}(ITE,1)-beaconx(I(ik),monte,sca))^2+(R{i,monte,j,z,zz,tri-(LS-1),sca}(ITE,2)-beacony(I(ik),monte,sca))^2)^(1/2)   ((2*R{i,monte,j,z,zz,tri-(LS-1),sca}(ITE,2)-2*beacony(I(ik),monte,sca))*(((R{i,monte,j,z,zz,tri-(LS-1),sca}(ITE,1)-beaconx(I(ik),monte,sca))^2+(R{i,monte,j,z,zz,tri-(LS-1),sca}(ITE,2)-beacony(I(ik),monte,sca))^2)^(1/2)-esti_dist(i,I(ik),monte,sca)))/((R{i,monte,j,z,zz,tri-(LS-1),sca}(ITE,1)-beaconx(I(ik),monte,sca))^2+(R{i,monte,j,z,zz,tri-(LS-1),sca}(ITE,2)-beacony(I(ik),monte,sca))^2)^(1/2)];
                                end
                                % R contains (x,y,norm) for each iteration
                                R{i,monte,j,z,zz,tri-(LS-1),sca}(ITE + 1, 1:2) = R{i,monte,j,z,zz,tri-(LS-1),sca}(ITE, 1:2) - ((Jf'*Jf)\Jf'*f)';
                                
                                % Norm, used for finding best initial guess
                                R{i,monte,j,z,zz,tri-(LS-1),sca}(ITE + 1, 3) = norm(Jf' * f, 2);
                                
                                % Check if another iteration is needed
                                if norm(Jf' * f, 2) < TOLERANCE
                                    break
                                end
                                ITE = ITE + 1;
                            end
                        end
                    end
                    
                    if length(noise_levels) == 1 && length(loss) == 1  % If multilateration is tested
                        for j = 1:size(R,1)
                            Rvec = [R{j,monte,1,z,zz,tri-(LS-1),sca}(end,3),R{j,monte,2,z,zz,tri-(LS-1),sca}(end,3),R{j,monte,3,z,zz,tri-(LS-1),sca}(end,3),R{j,monte,4,z,zz,tri-(LS-1),sca}(end,3)];
                            [Rmin,Rix] = min(Rvec);
                            for i = 1:size(rssi,1)
                                if R{j,monte,Rix,z,zz,tri-(LS-1),sca}(end,1)<=rooms(i,3,sca) && R{j,monte,Rix,z,zz,tri-(LS-1),sca}(end,1)>=rooms(i,1,sca) && R{j,monte,Rix,z,zz,tri-(LS-1),sca}(end,2)<=rooms(i,4,sca) && R{j,monte,Rix,z,zz,tri-(LS-1),sca}(end,2)>=rooms(i,2,sca)
                                    if i == j
                                        high_correct(sca)=high_correct(sca)+1;
                                        temp=temp+1;
                                    end
                                    connection1(j,monte,sca) = i;
                                end
                            end
                        end
                        correct_random(monte,sca) = temp/20;
                        ratio(monte,sca) = high_correct(sca)/(monte*20);
                        
                        if length(noise_levels) ~= 1 && length(loss) == 1   % If noise is tested
                            for j = 1:size(R,1)
                                Rvec = [R{j,monte,1,z,zz,tri-(LS-1),sca}(end,3),R{j,monte,2,z,zz,tri-(LS-1),sca}(end,3),R{j,monte,3,z,zz,tri-(LS-1),sca}(end,3),R{j,monte,4,z,zz,tri-(LS-1),sca}(end,3)];
                                [Rmin,Rix] = min(Rvec);
                                for i = 1:size(rssi,1)
                                    if R{j,monte,Rix,z,zz,tri-(LS-1),sca}(end,1)<=rooms(i,3) && R{j,monte,Rix,z,zz,tri-(LS-1),sca}(end,1)>=rooms(i,1) && R{j,monte,Rix,z,zz,tri-(LS-1),sca}(end,2)<=rooms(i,4) && R{j,monte,Rix,z,zz,tri-(LS-1),sca}(end,2)>=rooms(i,2)
                                        if i == j
                                            high_correct(z)=high_correct(z)+1;
                                            temp=temp+1;
                                        end
                                        connection1(j,monte,z) = i;
                                    end
                                end
                            end
                            correct_random(monte,z) = temp/20;
                            ratio(monte,z) = high_correct(z)/(monte*20);
                        elseif length(noise_levels) == 1 && length(loss) ~= 1  % If wall attenuation is tested
                            for j = 1:size(R,1)
                                Rvec = [R{j,monte,1,z,zz,tri-(LS-1),sca}(end,3),R{j,monte,2,z,zz,tri-(LS-1),sca}(end,3),R{j,monte,3,z,zz,tri-(LS-1),sca}(end,3),R{j,monte,4,z,zz,tri-(LS-1),sca}(end,3)];
                                [Rmin,Rix] = min(Rvec);
                                for i = 1:size(rssi,1)
                                    if R{j,monte,Rix,z,zz,tri-(LS-1),sca}(end,1)<=rooms(i,3) && R{j,monte,Rix,z,zz,tri-(LS-1),sca}(end,1)>=rooms(i,1) && R{j,monte,Rix,z,zz,tri-(LS-1),sca}(end,2)<=rooms(i,4) && R{j,monte,Rix,z,zz,tri-(LS-1),sca}(end,2)>=rooms(i,2)
                                        if i == j
                                            high_correct(zz)=high_correct(zz)+1;
                                            temp=temp+1;
                                        end
                                        connection1(j,monte,zz) = i;
                                    end
                                end
                            end
                            correct_random(monte,zz) = temp/20;
                            ratio(monte,zz) = high_correct(zz)/(monte*20);
                        elseif length(noise_levels) == 1 && length(loss) == 1  % If multilateration is tested
                            for j = 1:size(R,1)
                                Rvec = [R{j,monte,1,z,zz,tri-(LS-1),sca}(end,3),R{j,monte,2,z,zz,tri-(LS-1),sca}(end,3),R{j,monte,3,z,zz,tri-(LS-1),sca}(end,3),R{j,monte,4,z,zz,tri-(LS-1),sca}(end,3)];
                                [Rmin,Rix] = min(Rvec);
                                for i = 1:size(rssi,1)
                                    if R{j,monte,Rix,z,zz,tri-(LS-1),sca}(end,1)<=rooms(i,3) && R{j,monte,Rix,z,zz,tri-(LS-1),sca}(end,1)>=rooms(i,1) && R{j,monte,Rix,z,zz,tri-(LS-1),sca}(end,2)<=rooms(i,4) && R{j,monte,Rix,z,zz,tri-(LS-1),sca}(end,2)>=rooms(i,2)
                                        if i == j
                                            high_correct(tri-(LS-1))=high_correct(tri-(LS-1))+1;
                                            temp=temp+1;
                                        end
                                        connection1(j,monte,tri-(LS-1)) = i;
                                    end
                                end
                            end
                            correct_random(monte,tri-(LS-1)) = temp/20;
                            ratio(monte,tri-(LS-1)) = high_correct(tri-(LS-1))/(monte*20);
                        end
                    end
                end
            end
        end
    end
end
disp('--------------------------------------');
disp('Random without room correction is done');
disp('--------------------------------------');
toc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Non-Linear Least Square - Random - Room correction                            2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic
if length(noise_levels) ~= 1 && length(loss) == 1
    high_correct2 = zeros(1,length(noise_levels));
elseif length(noise_levels) == 1 && length(loss) ~= 1
    high_correct2 = zeros(1,length(loss));
elseif length(noise_levels) == 1 && length(loss) == 1
high_correct2 = zeros(1,scales_i);    
    %high_correct2 = zeros(1,LATERATION-(LS-1));
end

for sca = 1:scales_i
room_dist = zeros(size(rssi,1),1);
x_center(:,sca) = ((rooms(:,3,sca)-rooms(:,1,sca))/2)+rooms(:,1,sca);
y_center(:,sca) = ((rooms(:,4,sca)-rooms(:,2,sca))/2)+rooms(:,2,sca);
end
connection2 = connection1;


for zz = 1:length(loss)
    for z = 1:length(noise_levels)
        for monte = 1:realizations
            for sca = 1:scales_i
                for tri = LS:LATERATION
                    temp = 0;
                    
                    if length(noise_levels) == 1 && length(loss) == 1   % If noise is tested
                        for j = 1:size(R,1)
                            Rvec = [R{j,monte,1,z,zz,tri-(LS-1),sca}(end,3),R{j,monte,2,z,zz,tri-(LS-1),sca}(end,3),R{j,monte,3,z,zz,tri-(LS-1),sca}(end,3),R{j,monte,4,z,zz,tri-(LS-1),sca}(end,3)];
                            [Rmin,Rix] = min(Rvec);
                            if connection2(j,monte,sca) == 0
                                for i = 1:size(rssi,1)
                                    room_dist(i) = sqrt((R{j,monte,Rix,z,zz,tri-(LS-1),sca}(end,1)-x_center(i,sca))^2+(R{j,monte,Rix,z,zz,tri-(LS-1),sca}(end,2)-y_center(i,sca))^2);
                                end
                                [M,Index] = min(room_dist);
                                connection2(j,monte,sca) = Index;
                            end
                        end
                        
                        for i = 1:size(rssi,1)
                            if connection2(i,monte,sca) == i
                                high_correct2(sca)=high_correct2(sca)+1;
                                temp=temp+1;
                            end
                        end
                        correct_random_rc(monte,sca) = temp/20;
                        ratio(monte,sca) = high_correct2(sca)/(monte*20);
                        
                        if length(noise_levels) ~= 1 && length(loss) == 1   % If noise is tested
                            for j = 1:size(R,1)
                                Rvec = [R{j,monte,1,z,zz,tri-(LS-1)}(end,3),R{j,monte,2,z,zz,tri-(LS-1)}(end,3),R{j,monte,3,z,zz,tri-(LS-1)}(end,3),R{j,monte,4,z,zz,tri-(LS-1)}(end,3)];
                                [Rmin,Rix] = min(Rvec);
                                if connection2(j,monte,z) == 0
                                    for i = 1:size(rssi,1)
                                        room_dist(i) = sqrt((R{j,monte,Rix,z,zz,tri-(LS-1)}(end,1)-x_center(i))^2+(R{j,monte,Rix,z,zz,tri-(LS-1)}(end,2)-y_center(i))^2);
                                    end
                                    [M,Index] = min(room_dist);
                                    connection2(j,monte,z) = Index;
                                end
                            end
                            
                            for i = 1:size(rssi,1)
                                if connection2(i,monte,z) == i
                                    high_correct2(z)=high_correct2(z)+1;
                                    temp=temp+1;
                                end
                            end
                            correct_random_rc(monte,z) = temp/20;
                            ratio(monte,z) = high_correct2(z)/(monte*20);
                            
                        elseif length(noise_levels) == 1 && length(loss) ~= 1  % If wall attenuation is tested
                            for j = 1:size(R,1)
                                Rvec = [R{j,monte,1,z,zz,tri-(LS-1)}(end,3),R{j,monte,2,z,zz,tri-(LS-1)}(end,3),R{j,monte,3,z,zz,tri-(LS-1)}(end,3),R{j,monte,4,z,zz,tri-(LS-1)}(end,3)];
                                [Rmin,Rix] = min(Rvec);
                                if connection2(j,monte,zz) == 0
                                    for i = 1:size(rssi,1)
                                        room_dist(i) = sqrt((R{j,monte,Rix,z,zz,tri-(LS-1)}(end,1)-x_center(i))^2+(R{j,monte,Rix,z,zz,tri-(LS-1)}(end,2)-y_center(i))^2);
                                    end
                                    [M,Index] = min(room_dist);
                                    connection2(j,monte,zz) = Index;
                                end
                            end
                            
                            for i = 1:size(rssi,1)
                                if connection2(i,monte,zz) == i
                                    high_correct2(zz)=high_correct2(zz)+1;
                                    temp=temp+1;
                                end
                            end
                            correct_random_rc(monte,zz) = temp/20;
                            ratio(monte,zz) = high_correct2(zz)/(monte*20);
                            
                        elseif length(noise_levels) == 1 && length(loss) == 1  % If multilateration is tested
                            for j = 1:size(R,1)
                                Rvec = [R{j,monte,1,z,zz,tri-(LS-1)}(end,3),R{j,monte,2,z,zz,tri-(LS-1)}(end,3),R{j,monte,3,z,zz,tri-(LS-1)}(end,3),R{j,monte,4,z,zz,tri-(LS-1)}(end,3)];
                                [Rmin,Rix] = min(Rvec);
                                if connection2(j,monte,tri-(LS-1)) == 0
                                    for i = 1:size(rssi,1)
                                        room_dist(i) = sqrt((R{j,monte,Rix,z,zz,tri-(LS-1)}(end,1)-x_center(i))^2+(R{j,monte,Rix,z,zz,tri-(LS-1)}(end,2)-y_center(i))^2);
                                    end
                                    [M,Index] = min(room_dist);
                                    connection2(j,monte,tri-(LS-1)) = Index;
                                end
                            end
                            
                            for i = 1:size(rssi,1)
                                if connection2(i,monte,tri-(LS-1)) == i
                                    high_correct2(tri-(LS-1))=high_correct2(tri-(LS-1))+1;
                                    temp=temp+1;
                                end
                            end
                            correct_random_rc(monte,tri-(LS-1)) = temp/20;
                            ratio(monte,tri-(LS-1)) = high_correct2(tri-(LS-1))/(monte*20);
                        end
                    end
                end
            end
        end
    end
end
disp('-----------------------------------');
disp('Random with room correction is done');
disp('-----------------------------------');
toc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Non-Linear Least Square - Center                                              3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic
clear R
if length(noise_levels) ~= 1 && length(loss) == 1
    high_correct = zeros(1,length(noise_levels));
elseif length(noise_levels) == 1 && length(loss) ~= 1
    high_correct = zeros(1,length(loss));
elseif length(noise_levels) == 1 && length(loss) == 1
    high_correct = zeros(1,LATERATION-(LS-1));
end

for zz = 1:length(loss)
    for z = 1:length(noise_levels)
        rssi = rssi_clean_center-(n_center.*loss(zz))+noise(:,:,:,z);
        
        if DIST_METHOD == 1
            % Estimated distance
            esti_dist = 0.5*exp(-0.1120479364*rssi - 4.703918033);
        elseif DIST_METHOD == 2
            % Alternative way of estimating distance
            
            wall =([0.3426 0.3314 0.3300 0.3315 0.2957 0.2932 0.3035 0.2989 0.3178 0.3351 0.3541 0.3276 0.3350 0.3174 0.2878 0.2914 0.3378 0.3346 0.3359 0.3397]); % average loss for individual rooms
            for i =1:length(wall)
                esti_dist(i,:,:) =0.5/(exp(lambertw(0.05602396820*wall(i)*loss(zz)*exp(-0.1120479364*rssi(i,:,:) - 4.703772370)) + 0.1120479364*rssi(i,:,:) + 4.703772370));
            end
        end
        
        for monte = 1: realizations
            for tri = LS:LATERATION
                clear f Jf
                temp = 0;
                for i = 1:size(rssi,1)
                    [B,I] = maxk(rssi(i,:,monte),tri);
                    
                    % Initial guesses
                    R{i,monte,1,z,zz,tri-(LS-1)}(1,1:2) = [0 0];
                    R{i,monte,2,z,zz,tri-(LS-1)}(1,1:2) = [30 30];
                    R{i,monte,3,z,zz,tri-(LS-1)}(1,1:2) = [0 30];
                    R{i,monte,4,z,zz,tri-(LS-1)}(1,1:2) = [30 0];
                    for j = 1:4
                        ITE = 1;
                        
                        % Functions
                        for ik = 1:tri
                            f(ik,:) = ( sqrt( (R{i,monte,j,z,zz,tri-(LS-1)}(1,1)-beaconx_center(I(ik),monte))^2 + (R{i,monte,j,z,zz,tri-(LS-1)}(1,2)-beacony_center(I(ik),monte))^2 ) - esti_dist(i,I(ik),monte) )^2;
                        end
                        % Jacobian
                        for ik = 1:tri
                            Jf(ik,:) = [((2*R{i,monte,j,z,zz,tri-(LS-1)}(1,1)-2*beaconx_center(I(ik),monte))*(((R{i,monte,j,z,zz,tri-(LS-1)}(1,1)-beaconx_center(I(ik),monte))^2+(R{i,monte,j,z,zz,tri-(LS-1)}(1,2)-beacony_center(I(ik),monte))^2)^(1/2)-esti_dist(i,I(ik),monte)))/((R{i,monte,j,z,zz,tri-(LS-1)}(1,1)-beaconx_center(I(ik),monte))^2+(R{i,monte,j,z,zz,tri-(LS-1)}(1,2)-beacony_center(I(ik),monte))^2)^(1/2)   ((2*R{i,monte,j,z,zz,tri-(LS-1)}(1,2)-2*beacony_center(I(ik),monte))*(((R{i,monte,j,z,zz,tri-(LS-1)}(1,1)-beaconx_center(I(ik),monte))^2+(R{i,monte,j,z,zz,tri-(LS-1)}(1,2)-beacony_center(I(ik),monte))^2)^(1/2)-esti_dist(i,I(ik),monte)))/((R{i,monte,j,z,zz,tri-(LS-1)}(1,1)-beaconx_center(I(ik),monte))^2+(R{i,monte,j,z,zz,tri-(LS-1)}(1,2)-beacony_center(I(ik),monte))^2)^(1/2)];
                        end
                         
                        % Check if initial guess is sufficient
                        R{i,monte,j,z,zz,tri-(LS-1)}(1, 3) = norm(Jf' * f, 2);
                        if norm(Jf' * f, 2) < TOLERANCE
                            break
                        end
                        
                        % Gauss-Newton iteration, as long as we haven't hit the
                        % maximumm # of iterations and the norm > tolerance
                        while ITE < MAX_ITE %&& norm(Jf' * f, 2) > TOLERANCE
                            for ik = 1:tri
                                f(ik,:) = ( sqrt( (R{i,monte,j,z,zz,tri-(LS-1)}(ITE,1)-beaconx_center(I(ik),monte))^2 + (R{i,monte,j,z,zz,tri-(LS-1)}(ITE,2)-beacony_center(I(ik),monte))^2 ) - esti_dist(i,I(ik),monte) )^2;
                            end
                            for ik = 1:tri
                                Jf(ik,:) = [((2*R{i,monte,j,z,zz,tri-(LS-1)}(ITE,1)-2*beaconx_center(I(ik),monte))*(((R{i,monte,j,z,zz,tri-(LS-1)}(ITE,1)-beaconx_center(I(ik),monte))^2+(R{i,monte,j,z,zz,tri-(LS-1)}(ITE,2)-beacony_center(I(ik),monte))^2)^(1/2)-esti_dist(i,I(ik),monte)))/((R{i,monte,j,z,zz,tri-(LS-1)}(ITE,1)-beaconx_center(I(ik),monte))^2+(R{i,monte,j,z,zz,tri-(LS-1)}(ITE,2)-beacony_center(I(ik),monte))^2)^(1/2)   ((2*R{i,monte,j,z,zz,tri-(LS-1)}(ITE,2)-2*beacony_center(I(ik),monte))*(((R{i,monte,j,z,zz,tri-(LS-1)}(ITE,1)-beaconx_center(I(ik),monte))^2+(R{i,monte,j,z,zz,tri-(LS-1)}(ITE,2)-beacony_center(I(ik),monte))^2)^(1/2)-esti_dist(i,I(ik),monte)))/((R{i,monte,j,z,zz,tri-(LS-1)}(ITE,1)-beaconx_center(I(ik),monte))^2+(R{i,monte,j,z,zz,tri-(LS-1)}(ITE,2)-beacony_center(I(ik),monte))^2)^(1/2)];
                            end
                            % R contains (x,y,norm) for each iteration
                            R{i,monte,j,z,zz,tri-(LS-1)}(ITE + 1, 1:2) = R{i,monte,j,z,zz,tri-(LS-1)}(ITE, 1:2) - ((Jf'*Jf)\Jf'*f)';
                            
                            % Norm, used for finding best initial guess
                            R{i,monte,j,z,zz,tri-(LS-1)}(ITE + 1, 3) = norm(Jf' * f, 2);
                            
                            % Check if another iteration is needed
                            if norm(Jf' * f, 2) < TOLERANCE
                                break
                            end
                            ITE = ITE + 1;
                        end
                    end
                end
                
                if length(noise_levels) ~= 1 && length(loss) == 1   % If noise is tested
                    for j = 1:size(R,1)
                        Rvec = [R{j,monte,1,z,zz,tri-(LS-1)}(end,3),R{j,monte,2,z,zz,tri-(LS-1)}(end,3),R{j,monte,3,z,zz,tri-(LS-1)}(end,3),R{j,monte,4,z,zz,tri-(LS-1)}(end,3)];
                        [Rmin,Rix] = min(Rvec);
                        for i = 1:size(rssi,1)
                            if R{j,monte,Rix,z,zz,tri-(LS-1)}(end,1)<=rooms(i,3) && R{j,monte,Rix,z,zz,tri-(LS-1)}(end,1)>=rooms(i,1) && R{j,monte,Rix,z,zz,tri-(LS-1)}(end,2)<=rooms(i,4) && R{j,monte,Rix,z,zz,tri-(LS-1)}(end,2)>=rooms(i,2)
                                if i == j
                                    high_correct(z)=high_correct(z)+1;
                                    temp=temp+1;
                                end
                                connection3(j,monte,z) = i;
                            end
                        end
                    end
                    correct_center(monte,z) = temp/20;
                    ratio(monte,z) = high_correct(z)/(monte*20);
                elseif length(noise_levels) == 1 && length(loss) ~= 1  % If wall attenuation is tested
                    for j = 1:size(R,1)
                        Rvec = [R{j,monte,1,z,zz,tri-(LS-1)}(end,3),R{j,monte,2,z,zz,tri-(LS-1)}(end,3),R{j,monte,3,z,zz,tri-(LS-1)}(end,3),R{j,monte,4,z,zz,tri-(LS-1)}(end,3)];
                        [Rmin,Rix] = min(Rvec);
                        for i = 1:size(rssi,1)
                            if R{j,monte,Rix,z,zz,tri-(LS-1)}(end,1)<=rooms(i,3) && R{j,monte,Rix,z,zz,tri-(LS-1)}(end,1)>=rooms(i,1) && R{j,monte,Rix,z,zz,tri-(LS-1)}(end,2)<=rooms(i,4) && R{j,monte,Rix,z,zz,tri-(LS-1)}(end,2)>=rooms(i,2)
                                if i == j
                                    high_correct(zz)=high_correct(zz)+1;
                                    temp=temp+1;
                                end
                                connection3(j,monte,zz) = i;
                            end
                        end
                    end
                    correct_center(monte,zz) = temp/20;
                    ratio(monte,zz) = high_correct(zz)/(monte*20);
                elseif length(noise_levels) == 1 && length(loss) == 1  % If multilateration is tested
                    for j = 1:size(R,1)
                        Rvec = [R{j,monte,1,z,zz,tri-(LS-1)}(end,3),R{j,monte,2,z,zz,tri-(LS-1)}(end,3),R{j,monte,3,z,zz,tri-(LS-1)}(end,3),R{j,monte,4,z,zz,tri-(LS-1)}(end,3)];
                        [Rmin,Rix] = min(Rvec);
                        for i = 1:size(rssi,1)
                            if R{j,monte,Rix,z,zz,tri-(LS-1)}(end,1)<=rooms(i,3) && R{j,monte,Rix,z,zz,tri-(LS-1)}(end,1)>=rooms(i,1) && R{j,monte,Rix,z,zz,tri-(LS-1)}(end,2)<=rooms(i,4) && R{j,monte,Rix,z,zz,tri-(LS-1)}(end,2)>=rooms(i,2)
                                if i == j
                                    high_correct(tri-(LS-1))=high_correct(tri-(LS-1))+1;
                                    temp=temp+1;
                                end
                                connection3(j,monte,tri-(LS-1)) = i;
                            end
                        end
                    end
                    correct_center(monte,tri-(LS-1)) = temp/20;
                    ratio(monte,tri-(LS-1)) = high_correct(tri-(LS-1))/(monte*20);
                end
            end
        end
    end
end
disp('--------------------------------------');
disp('Center without room correction is done');
disp('--------------------------------------');
toc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Non-Linear Least Square - Center - Room correction                            4
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic
if length(noise_levels) ~= 1 && length(loss) == 1
    high_correct2 = zeros(1,length(noise_levels));
elseif length(noise_levels) == 1 && length(loss) ~= 1
    high_correct2 = zeros(1,length(loss));
elseif length(noise_levels) == 1 && length(loss) == 1
    high_correct2 = zeros(1,LATERATION-(LS-1));
end

room_dist = zeros(size(rssi,1),1);
x_center = ((rooms(:,3)-rooms(:,1))/2)+rooms(:,1);
y_center = ((rooms(:,4)-rooms(:,2))/2)+rooms(:,2);
connection4 = connection3;

for zz = 1:length(loss)
    for z = 1:length(noise_levels)
        for monte = 1:realizations
            for tri = LS:LATERATION
                temp = 0;
                
                if length(noise_levels) ~= 1 && length(loss) == 1   % If noise is tested
                    for j = 1:size(R,1)
                        Rvec = [R{j,monte,1,z,zz,tri-(LS-1)}(end,3),R{j,monte,2,z,zz,tri-(LS-1)}(end,3),R{j,monte,3,z,zz,tri-(LS-1)}(end,3),R{j,monte,4,z,zz,tri-(LS-1)}(end,3)];
                        [Rmin,Rix] = min(Rvec);
                        if connection4(j,monte,z) == 0
                            for i = 1:size(rssi,1)
                                room_dist(i) = sqrt((R{j,monte,Rix,z,zz,tri-(LS-1)}(end,1)-x_center(i))^2+(R{j,monte,Rix,z,zz,tri-(LS-1)}(end,2)-y_center(i))^2);
                            end
                            [M,Index] = min(room_dist);
                            connection4(j,monte,z) = Index;
                        end
                    end
                    
                    for i = 1:size(rssi,1)
                        if connection4(i,monte,z) == i
                            high_correct2(z)=high_correct2(z)+1;
                            temp=temp+1;
                        end
                    end
                    correct_center_rc(monte,z) = temp/20;
                    ratio(monte,z) = high_correct2(z)/(monte*20);
                    
                elseif length(noise_levels) == 1 && length(loss) ~= 1  % If wall attenuation is tested
                    for j = 1:size(R,1)
                        Rvec = [R{j,monte,1,z,zz,tri-(LS-1)}(end,3),R{j,monte,2,z,zz,tri-(LS-1)}(end,3),R{j,monte,3,z,zz,tri-(LS-1)}(end,3),R{j,monte,4,z,zz,tri-(LS-1)}(end,3)];
                        [Rmin,Rix] = min(Rvec);
                        if connection4(j,monte,zz) == 0
                            for i = 1:size(rssi,1)
                                room_dist(i) = sqrt((R{j,monte,Rix,z,zz,tri-(LS-1)}(end,1)-x_center(i))^2+(R{j,monte,Rix,z,zz,tri-(LS-1)}(end,2)-y_center(i))^2);
                            end
                            [M,Index] = min(room_dist);
                            connection4(j,monte,zz) = Index;
                        end
                    end
                    
                    for i = 1:size(rssi,1)
                        if connection4(i,monte,zz) == i
                            high_correct2(zz)=high_correct2(zz)+1;
                            temp=temp+1;
                        end
                    end
                    correct_center_rc(monte,zz) = temp/20;
                    ratio(monte,zz) = high_correct2(zz)/(monte*20);
                    
                elseif length(noise_levels) == 1 && length(loss) == 1  % If multilateration is tested
                    for j = 1:size(R,1)
                        Rvec = [R{j,monte,1,z,zz,tri-(LS-1)}(end,3),R{j,monte,2,z,zz,tri-(LS-1)}(end,3),R{j,monte,3,z,zz,tri-(LS-1)}(end,3),R{j,monte,4,z,zz,tri-(LS-1)}(end,3)];
                        [Rmin,Rix] = min(Rvec);
                        if connection4(j,monte,tri-(LS-1)) == 0
                            for i = 1:size(rssi,1)
                                room_dist(i) = sqrt((R{j,monte,Rix,z,zz,tri-(LS-1)}(end,1)-x_center(i))^2+(R{j,monte,Rix,z,zz,tri-(LS-1)}(end,2)-y_center(i))^2);
                            end
                            [M,Index] = min(room_dist);
                            connection4(j,monte,tri-(LS-1)) = Index;
                        end
                    end
                    
                    for i = 1:size(rssi,1)
                        if connection4(i,monte,tri-(LS-1)) == i
                            high_correct2(tri-(LS-1))=high_correct2(tri-(LS-1))+1;
                            temp=temp+1;
                        end
                    end
                    correct_center_rc(monte,tri-(LS-1)) = temp/20;
                    ratio(monte,tri-(LS-1)) = high_correct2(tri-(LS-1))/(monte*20);
                end
            end
        end
    end
end
disp('-----------------------------------');
disp('Center with room correction is done');
disp('-----------------------------------');
toc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Non-Linear Least Square - Corner                                              5
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic
clear R
if length(noise_levels) ~= 1 && length(loss) == 1
    high_correct = zeros(1,length(noise_levels));
elseif length(noise_levels) == 1 && length(loss) ~= 1
    high_correct = zeros(1,length(loss));
elseif length(noise_levels) == 1 && length(loss) == 1
    high_correct = zeros(1,LATERATION-(LS-1));
end

for zz = 1:length(loss)
    for z = 1:length(noise_levels)
        rssi = rssi_clean_corner-(n_corner.*loss(zz))+noise(:,:,:,z);
        
        if DIST_METHOD == 1
            % Estimated distance
            esti_dist = 0.5*exp(-0.1120479364*rssi - 4.703918033);
        elseif DIST_METHOD == 2
            % Alternative way of estimating distance
            
            wall =([0.3426 0.3314 0.3300 0.3315 0.2957 0.2932 0.3035 0.2989 0.3178 0.3351 0.3541 0.3276 0.3350 0.3174 0.2878 0.2914 0.3378 0.3346 0.3359 0.3397]); % average loss for individual rooms
            for i =1:length(wall)
                esti_dist(i,:,:) =0.5/(exp(lambertw(0.05602396820*wall(i)*loss(zz)*exp(-0.1120479364*rssi(i,:,:) - 4.703772370)) + 0.1120479364*rssi(i,:,:) + 4.703772370));
            end
        end
        
        for monte = 1: realizations
            for tri = LS:LATERATION
                clear f Jf
                temp = 0;
                for i = 1:size(rssi,1)
                    [B,I] = maxk(rssi(i,:,monte),tri);
                    
                    % Initial guesses
                    R{i,monte,1,z,zz,tri-(LS-1)}(1,1:2) = [0 0];
                    R{i,monte,2,z,zz,tri-(LS-1)}(1,1:2) = [30 30];
                    R{i,monte,3,z,zz,tri-(LS-1)}(1,1:2) = [0 30];
                    R{i,monte,4,z,zz,tri-(LS-1)}(1,1:2) = [30 0];
                    for j = 1:4
                        ITE = 1;
                        
                        % Functions
                        for ik = 1:tri
                            f(ik,:) = ( sqrt( (R{i,monte,j,z,zz,tri-(LS-1)}(1,1)-beaconx_corner(I(ik),monte))^2 + (R{i,monte,j,z,zz,tri-(LS-1)}(1,2)-beacony_corner(I(ik),monte))^2 ) - esti_dist(i,I(ik),monte) )^2;
                        end
                        % Jacobian
                        for ik = 1:tri
                            Jf(ik,:) = [((2*R{i,monte,j,z,zz,tri-(LS-1)}(1,1)-2*beaconx_corner(I(ik),monte))*(((R{i,monte,j,z,zz,tri-(LS-1)}(1,1)-beaconx_corner(I(ik),monte))^2+(R{i,monte,j,z,zz,tri-(LS-1)}(1,2)-beacony_corner(I(ik),monte))^2)^(1/2)-esti_dist(i,I(ik),monte)))/((R{i,monte,j,z,zz,tri-(LS-1)}(1,1)-beaconx_corner(I(ik),monte))^2+(R{i,monte,j,z,zz,tri-(LS-1)}(1,2)-beacony_corner(I(ik),monte))^2)^(1/2)   ((2*R{i,monte,j,z,zz,tri-(LS-1)}(1,2)-2*beacony_corner(I(ik),monte))*(((R{i,monte,j,z,zz,tri-(LS-1)}(1,1)-beaconx_corner(I(ik),monte))^2+(R{i,monte,j,z,zz,tri-(LS-1)}(1,2)-beacony_corner(I(ik),monte))^2)^(1/2)-esti_dist(i,I(ik),monte)))/((R{i,monte,j,z,zz,tri-(LS-1)}(1,1)-beaconx_corner(I(ik),monte))^2+(R{i,monte,j,z,zz,tri-(LS-1)}(1,2)-beacony_corner(I(ik),monte))^2)^(1/2)];
                        end
                         
                        % Check if initial guess is sufficient
                        R{i,monte,j,z,zz,tri-(LS-1)}(1, 3) = norm(Jf' * f, 2);
                        if norm(Jf' * f, 2) < TOLERANCE
                            break
                        end
                        
                        % Gauss-Newton iteration, as long as we haven't hit the
                        % maximumm # of iterations and the norm > tolerance
                        while ITE < MAX_ITE %&& norm(Jf' * f, 2) > TOLERANCE
                            for ik = 1:tri
                                f(ik,:) = ( sqrt( (R{i,monte,j,z,zz,tri-(LS-1)}(ITE,1)-beaconx_corner(I(ik),monte))^2 + (R{i,monte,j,z,zz,tri-(LS-1)}(ITE,2)-beacony_corner(I(ik),monte))^2 ) - esti_dist(i,I(ik),monte) )^2;
                            end
                            for ik = 1:tri
                                Jf(ik,:) = [((2*R{i,monte,j,z,zz,tri-(LS-1)}(ITE,1)-2*beaconx_corner(I(ik),monte))*(((R{i,monte,j,z,zz,tri-(LS-1)}(ITE,1)-beaconx_corner(I(ik),monte))^2+(R{i,monte,j,z,zz,tri-(LS-1)}(ITE,2)-beacony_corner(I(ik),monte))^2)^(1/2)-esti_dist(i,I(ik),monte)))/((R{i,monte,j,z,zz,tri-(LS-1)}(ITE,1)-beaconx_corner(I(ik),monte))^2+(R{i,monte,j,z,zz,tri-(LS-1)}(ITE,2)-beacony_corner(I(ik),monte))^2)^(1/2)   ((2*R{i,monte,j,z,zz,tri-(LS-1)}(ITE,2)-2*beacony_corner(I(ik),monte))*(((R{i,monte,j,z,zz,tri-(LS-1)}(ITE,1)-beaconx_corner(I(ik),monte))^2+(R{i,monte,j,z,zz,tri-(LS-1)}(ITE,2)-beacony_corner(I(ik),monte))^2)^(1/2)-esti_dist(i,I(ik),monte)))/((R{i,monte,j,z,zz,tri-(LS-1)}(ITE,1)-beaconx_corner(I(ik),monte))^2+(R{i,monte,j,z,zz,tri-(LS-1)}(ITE,2)-beacony_corner(I(ik),monte))^2)^(1/2)];
                            end
                            % R contains (x,y,norm) for each iteration
                            R{i,monte,j,z,zz,tri-(LS-1)}(ITE + 1, 1:2) = R{i,monte,j,z,zz,tri-(LS-1)}(ITE, 1:2) - ((Jf'*Jf)\Jf'*f)';
                            
                            % Norm, used for finding best initial guess
                            R{i,monte,j,z,zz,tri-(LS-1)}(ITE + 1, 3) = norm(Jf' * f, 2);
                            
                            % Check if another iteration is needed
                            if norm(Jf' * f, 2) < TOLERANCE
                                break
                            end
                            ITE = ITE + 1;
                        end
                    end
                end
                
                if length(noise_levels) ~= 1 && length(loss) == 1   % If noise is tested
                    for j = 1:size(R,1)
                        Rvec = [R{j,monte,1,z,zz,tri-(LS-1)}(end,3),R{j,monte,2,z,zz,tri-(LS-1)}(end,3),R{j,monte,3,z,zz,tri-(LS-1)}(end,3),R{j,monte,4,z,zz,tri-(LS-1)}(end,3)];
                        [Rmin,Rix] = min(Rvec);
                        for i = 1:size(rssi,1)
                            if R{j,monte,Rix,z,zz,tri-(LS-1)}(end,1)<=rooms(i,3) && R{j,monte,Rix,z,zz,tri-(LS-1)}(end,1)>=rooms(i,1) && R{j,monte,Rix,z,zz,tri-(LS-1)}(end,2)<=rooms(i,4) && R{j,monte,Rix,z,zz,tri-(LS-1)}(end,2)>=rooms(i,2)
                                if i == j
                                    high_correct(z)=high_correct(z)+1;
                                    temp=temp+1;
                                end
                                connection5(j,monte,z) = i;
                            end
                        end
                    end
                    correct_corner(monte,z) = temp/20;
                    ratio(monte,z) = high_correct(z)/(monte*20);
                elseif length(noise_levels) == 1 && length(loss) ~= 1  % If wall attenuation is tested
                    for j = 1:size(R,1)
                        Rvec = [R{j,monte,1,z,zz,tri-(LS-1)}(end,3),R{j,monte,2,z,zz,tri-(LS-1)}(end,3),R{j,monte,3,z,zz,tri-(LS-1)}(end,3),R{j,monte,4,z,zz,tri-(LS-1)}(end,3)];
                        [Rmin,Rix] = min(Rvec);
                        for i = 1:size(rssi,1)
                            if R{j,monte,Rix,z,zz,tri-(LS-1)}(end,1)<=rooms(i,3) && R{j,monte,Rix,z,zz,tri-(LS-1)}(end,1)>=rooms(i,1) && R{j,monte,Rix,z,zz,tri-(LS-1)}(end,2)<=rooms(i,4) && R{j,monte,Rix,z,zz,tri-(LS-1)}(end,2)>=rooms(i,2)
                                if i == j
                                    high_correct(zz)=high_correct(zz)+1;
                                    temp=temp+1;
                                end
                                connection5(j,monte,zz) = i;
                            end
                        end
                    end
                    correct_corner(monte,zz) = temp/20;
                    ratio(monte,zz) = high_correct(zz)/(monte*20);
                elseif length(noise_levels) == 1 && length(loss) == 1  % If multilateration is tested
                    for j = 1:size(R,1)
                        Rvec = [R{j,monte,1,z,zz,tri-(LS-1)}(end,3),R{j,monte,2,z,zz,tri-(LS-1)}(end,3),R{j,monte,3,z,zz,tri-(LS-1)}(end,3),R{j,monte,4,z,zz,tri-(LS-1)}(end,3)];
                        [Rmin,Rix] = min(Rvec);
                        for i = 1:size(rssi,1)
                            if R{j,monte,Rix,z,zz,tri-(LS-1)}(end,1)<=rooms(i,3) && R{j,monte,Rix,z,zz,tri-(LS-1)}(end,1)>=rooms(i,1) && R{j,monte,Rix,z,zz,tri-(LS-1)}(end,2)<=rooms(i,4) && R{j,monte,Rix,z,zz,tri-(LS-1)}(end,2)>=rooms(i,2)
                                if i == j
                                    high_correct(tri-(LS-1))=high_correct(tri-(LS-1))+1;
                                    temp=temp+1;
                                end
                                connection5(j,monte,tri-(LS-1)) = i;
                            end
                        end
                    end
                    correct_corner(monte,tri-(LS-1)) = temp/20;
                    ratio(monte,tri-(LS-1)) = high_correct(tri-(LS-1))/(monte*20);
                end
            end
        end
    end
end
disp('--------------------------------------');
disp('Corner with outroom correction is done');
disp('--------------------------------------');
toc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Non-Linear Least Square - Corner - Room correction                            6
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tic
if length(noise_levels) ~= 1 && length(loss) == 1
    high_correct2 = zeros(1,length(noise_levels));
elseif length(noise_levels) == 1 && length(loss) ~= 1
    high_correct2 = zeros(1,length(loss));
elseif length(noise_levels) == 1 && length(loss) == 1
    high_correct2 = zeros(1,LATERATION-(LS-1));
end

room_dist = zeros(size(rssi,1),1);
x_center = ((rooms(:,3)-rooms(:,1))/2)+rooms(:,1);
y_center = ((rooms(:,4)-rooms(:,2))/2)+rooms(:,2);
connection6 = connection5;

for zz = 1:length(loss)
    for z = 1:length(noise_levels)
        for monte = 1:realizations
            for tri = LS:LATERATION
                temp = 0;
                
                if length(noise_levels) ~= 1 && length(loss) == 1   % If noise is tested
                    for j = 1:size(R,1)
                        Rvec = [R{j,monte,1,z,zz,tri-(LS-1)}(end,3),R{j,monte,2,z,zz,tri-(LS-1)}(end,3),R{j,monte,3,z,zz,tri-(LS-1)}(end,3),R{j,monte,4,z,zz,tri-(LS-1)}(end,3)];
                        [Rmin,Rix] = min(Rvec);
                        if connection6(j,monte,z) == 0
                            for i = 1:size(rssi,1)
                                room_dist(i) = sqrt((R{j,monte,Rix,z,zz,tri-(LS-1)}(end,1)-x_center(i))^2+(R{j,monte,Rix,z,zz,tri-(LS-1)}(end,2)-y_center(i))^2);
                            end
                            [M,Index] = min(room_dist);
                            connection6(j,monte,z) = Index;
                        end
                    end
                    
                    for i = 1:size(rssi,1)
                        if connection6(i,monte,z) == i
                            high_correct2(z)=high_correct2(z)+1;
                            temp=temp+1;
                        end
                    end
                    correct_corner_rc(monte,z) = temp/20;
                    ratio(monte,z) = high_correct2(z)/(monte*20);
                    
                elseif length(noise_levels) == 1 && length(loss) ~= 1  % If wall attenuation is tested
                    for j = 1:size(R,1)
                        Rvec = [R{j,monte,1,z,zz,tri-(LS-1)}(end,3),R{j,monte,2,z,zz,tri-(LS-1)}(end,3),R{j,monte,3,z,zz,tri-(LS-1)}(end,3),R{j,monte,4,z,zz,tri-(LS-1)}(end,3)];
                        [Rmin,Rix] = min(Rvec);
                        if connection6(j,monte,zz) == 0
                            for i = 1:size(rssi,1)
                                room_dist(i) = sqrt((R{j,monte,Rix,z,zz,tri-(LS-1)}(end,1)-x_center(i))^2+(R{j,monte,Rix,z,zz,tri-(LS-1)}(end,2)-y_center(i))^2);
                            end
                            [M,Index] = min(room_dist);
                            connection6(j,monte,zz) = Index;
                        end
                    end
                    
                    for i = 1:size(rssi,1)
                        if connection6(i,monte,zz) == i
                            high_correct2(zz)=high_correct2(zz)+1;
                            temp=temp+1;
                        end
                    end
                    correct_corner_rc(monte,zz) = temp/20;
                    ratio(monte,zz) = high_correct2(zz)/(monte*20);
                    
                elseif length(noise_levels) == 1 && length(loss) == 1  % If multilateration is tested
                    for j = 1:size(R,1)
                        Rvec = [R{j,monte,1,z,zz,tri-(LS-1)}(end,3),R{j,monte,2,z,zz,tri-(LS-1)}(end,3),R{j,monte,3,z,zz,tri-(LS-1)}(end,3),R{j,monte,4,z,zz,tri-(LS-1)}(end,3)];
                        [Rmin,Rix] = min(Rvec);
                        if connection6(j,monte,tri-(LS-1)) == 0
                            for i = 1:size(rssi,1)
                                room_dist(i) = sqrt((R{j,monte,Rix,z,zz,tri-(LS-1)}(end,1)-x_center(i))^2+(R{j,monte,Rix,z,zz,tri-(LS-1)}(end,2)-y_center(i))^2);
                            end
                            [M,Index] = min(room_dist);
                            connection6(j,monte,tri-(LS-1)) = Index;
                        end
                    end
                    
                    for i = 1:size(rssi,1)
                        if connection6(i,monte,tri-(LS-1)) == i
                            high_correct2(tri-(LS-1))=high_correct2(tri-(LS-1))+1;
                            temp=temp+1;
                        end
                    end
                    correct_corner_rc(monte,tri-(LS-1)) = temp/20;
                    ratio(monte,tri-(LS-1)) = high_correct2(tri-(LS-1))/(monte*20);
                end
            end
        end
    end
end
disp('-----------------------------------');
disp('Corner with room correction is done');
disp('-----------------------------------');
toc
warning('on', 'MATLAB:nearlySingularMatrix');
%% Plotting
z = 2.576; % equal to 99% interval
sigma_random = std(correct_random);
sigma_center = std(correct_center);
sigma_corner = std(correct_corner);
sigma_random_rc = std(correct_random_rc);
sigma_center_rc = std(correct_center_rc);
sigma_corner_rc = std(correct_corner_rc);

%calc lower 
lower_random = mean(correct_random)-z*(sigma_random/sqrt(realizations));
lower_center = mean(correct_center)-z*(sigma_center/sqrt(realizations));
lower_corner = mean(correct_corner)-z*(sigma_corner/sqrt(realizations));
lower_random_rc = mean(correct_random_rc)-z*(sigma_random_rc/sqrt(realizations));
lower_center_rc = mean(correct_center_rc)-z*(sigma_center_rc/sqrt(realizations));
lower_corner_rc = mean(correct_corner_rc)-z*(sigma_corner_rc/sqrt(realizations));

%calc upper
upper_random = mean(correct_random)+z*(sigma_random/sqrt(realizations));
upper_center = mean(correct_center)+z*(sigma_center/sqrt(realizations));
upper_corner = mean(correct_corner)+z*(sigma_corner/sqrt(realizations));
upper_random_rc = mean(correct_random_rc)+z*(sigma_random_rc/sqrt(realizations));
upper_center_rc = mean(correct_center_rc)+z*(sigma_center_rc/sqrt(realizations));
upper_corner_rc = mean(correct_corner_rc)+z*(sigma_corner_rc/sqrt(realizations));

if length(noise_levels) ~= 1 && length(loss) == 1
    if realizations > 1
        figure()
        hold on

        yy = [lower_random,fliplr(upper_random)];   % vector of upper & lower boundaries
        fill([noise_levels',fliplr(noise_levels')],yy,'b','edgealpha',0,'facealpha',.3) 

        yy = [lower_center,fliplr(upper_center)];   % vector of upper & lower boundaries
        fill([noise_levels',fliplr(noise_levels')],yy,'r','edgealpha',0,'facealpha',.3) 

        yy = [lower_corner,fliplr(upper_corner)];   % vector of upper & lower boundaries
        fill([noise_levels',fliplr(noise_levels')],yy,'m','edgealpha',0,'facealpha',.3) 

        %plot mean 
        plot(noise_levels,mean(correct_random),'b-o')
        plot(noise_levels,mean(correct_center),'r-o')
        plot(noise_levels,mean(correct_corner),'m-o')
        xline(22.159,'--');
        xlim([0 noise_levels(end)])
        ylim([0 1])
        legend('Random','Center','Corner')
        title('Accuracy ratio over variance of noise')
        xlabel('Noise variance')
        ylabel('Accuracy ratio')
        grid
    end

    if realizations > 1
        figure()
        hold on

        yy = [lower_random_rc,fliplr(upper_random_rc)];   % vector of upper & lower boundaries
        fill([noise_levels',fliplr(noise_levels')],yy,'b','edgealpha',0,'facealpha',.3) 

        yy = [lower_center_rc,fliplr(upper_center_rc)];   % vector of upper & lower boundaries
        fill([noise_levels',fliplr(noise_levels')],yy,'r','edgealpha',0,'facealpha',.3) 

        yy = [lower_corner_rc,fliplr(upper_corner_rc)];   % vector of upper & lower boundaries
        fill([noise_levels',fliplr(noise_levels')],yy,'m','edgealpha',0,'facealpha',.3) 

        %plot mean 
        plot(noise_levels,mean(correct_random_rc),'b-o')
        plot(noise_levels,mean(correct_center_rc),'r-o')
        plot(noise_levels,mean(correct_corner_rc),'m-o')
        xline(22.159,'--');
        xlim([0 noise_levels(end)])
        ylim([0 1])
        legend('Random corrected','Center corrected','Corner corrected')
        title('Accuracy ratio over variance of noise')
        xlabel('Noise variance')
        ylabel('Accuracy ratio')
        grid
    end
elseif length(noise_levels) == 1 && length(loss) ~= 1
    if realizations > 1
        figure()
        hold on
        
        yy = [lower_random,fliplr(upper_random)];   % vector of upper & lower boundaries
        fill([loss',fliplr(loss')],yy,'b','edgealpha',0,'facealpha',.3)
        
        yy = [lower_center,fliplr(upper_center)];   % vector of upper & lower boundaries
        fill([loss',fliplr(loss')],yy,'r','edgealpha',0,'facealpha',.3)
        
        yy = [lower_corner,fliplr(upper_corner)];   % vector of upper & lower boundaries
        fill([loss',fliplr(loss')],yy,'m','edgealpha',0,'facealpha',.3)
        
        %plot mean
        plot(loss,mean(correct_random),'b-o')
        plot(loss,mean(correct_center),'r-o')
        plot(loss,mean(correct_corner),'m-o')
        xline(2,'--');
        xlim([0 loss(end)])
        ylim([0 1])
        legend('Random','Center','Corner')
        title('Accuracy ratio over dB loss')
        xlabel('dB loss per wall')
        ylabel('Accuracy ratio')
        grid
    end
    
    if realizations > 1
        figure()
        hold on
        
        yy = [lower_random_rc,fliplr(upper_random_rc)];   % vector of upper & lower boundaries
        fill([loss',fliplr(loss')],yy,'b','edgealpha',0,'facealpha',.3)
        
        yy = [lower_center_rc,fliplr(upper_center_rc)];   % vector of upper & lower boundaries
        fill([loss',fliplr(loss')],yy,'r','edgealpha',0,'facealpha',.3)
        
        yy = [lower_corner_rc,fliplr(upper_corner_rc)];   % vector of upper & lower boundaries
        fill([loss',fliplr(loss')],yy,'m','edgealpha',0,'facealpha',.3)
        
        %plot mean
        plot(loss,mean(correct_random_rc),'b-o')
        plot(loss,mean(correct_center_rc),'r-o')
        plot(loss,mean(correct_corner_rc),'m-o')
        xline(2,'--');
        xlim([0 loss(end)])
        ylim([0 1])
        legend('Random corrected','Center corrected','Corner corrected')
        title('Accuracy ratio over dB loss')
        xlabel('dB loss per wall')
        ylabel('Accuracy ratio')
        grid
    end    
elseif length(noise_levels) == 1 && length(loss) == 1
    if realizations > 1
        figure()
        hold on
        
        yy = [lower_random,fliplr(upper_random)];   % vector of upper & lower boundaries
        fill([LV',fliplr(LV')],yy,'b','edgealpha',0,'facealpha',.3)
        
        yy = [lower_center,fliplr(upper_center)];   % vector of upper & lower boundaries
        fill([LV',fliplr(LV')],yy,'r','edgealpha',0,'facealpha',.3)
        
        yy = [lower_corner,fliplr(upper_corner)];   % vector of upper & lower boundaries
        fill([LV',fliplr(LV')],yy,'m','edgealpha',0,'facealpha',.3)
        
        %plot mean
        plot(LV,mean(correct_random),'b-o')
        plot(LV,mean(correct_center),'r-o')
        plot(LV,mean(correct_corner),'m-o')
        xline(2,'--');
        xlim([LS LV(end)])
        ylim([0 1])
        legend('Random','Center','Corner')
        title('Accuracy ratio varying degrees of multilateration')
        xlabel('Number of devices used for multilateration')
        ylabel('Accuracy ratio')
        grid
    end
    
    if realizations > 1
        figure()
        hold on
        
        yy = [lower_random_rc,fliplr(upper_random_rc)];   % vector of upper & lower boundaries
        fill([LV',fliplr(LV')],yy,'b','edgealpha',0,'facealpha',.3)
        
        yy = [lower_center_rc,fliplr(upper_center_rc)];   % vector of upper & lower boundaries
        fill([LV',fliplr(LV')],yy,'r','edgealpha',0,'facealpha',.3)
        
        yy = [lower_corner_rc,fliplr(upper_corner_rc)];   % vector of upper & lower boundaries
        fill([LV',fliplr(LV')],yy,'m','edgealpha',0,'facealpha',.3)
        
        %plot mean
        plot(LV,mean(correct_random_rc),'b-o')
        plot(LV,mean(correct_center_rc),'r-o')
        plot(LV,mean(correct_corner_rc),'m-o')
        xline(2,'--');
        xlim([LS LV(end)])
        ylim([0 1])
        legend('Random corrected','Center corrected','Corner corrected')
        title('Accuracy ratio varying degrees of multilateration')
        xlabel('Number of devices used for multilateration')
        ylabel('Accuracy ratio')
        grid
    end
end

%%
z = 2.576; % equal to 99% interval
sigma_random = std(correct_random);
sigma_random_rc = std(correct_random_rc);

%calc lower 
lower_random = mean(correct_random)-z*(sigma_random/sqrt(realizations));
lower_random_rc = mean(correct_random_rc)-z*(sigma_random_rc/sqrt(realizations));

%calc upper
upper_random = mean(correct_random)+z*(sigma_random/sqrt(realizations));
upper_random_rc = mean(correct_random_rc)+z*(sigma_random_rc/sqrt(realizations));

if length(noise_levels) == 1 && length(loss) == 1
    if realizations > 1
        figure()
        hold on
        
        yy = [lower_random,fliplr(upper_random)];   % vector of upper & lower boundaries
        fill([scales,fliplr(scales)],yy,'b','edgealpha',0,'facealpha',.3)
        
        %plot mean
        plot(scales,mean(correct_random),'b-o')
        xlim([1 scales(end)])
        ylim([0 1])
        legend('Random')
        title('Accuracy ratio varying sizes of building')
        xlabel('Scaling factor')
        ylabel('Accuracy ratio')
        grid
    end
    
    if realizations > 1
        figure()
        hold on
        
        yy = [lower_random_rc,fliplr(upper_random_rc)];   % vector of upper & lower boundaries
        fill([scales,fliplr(scales)],yy,'b','edgealpha',0,'facealpha',.3)
        
        %plot mean
        plot(scales,mean(correct_random_rc),'b-o')
        xlim([1 scales(end)])
        ylim([0 1])
        legend('Random corrected')
        title('Accuracy ratio varying sizes of builsing')
        xlabel('Scaling factor')
        ylabel('Accuracy ratio')
        grid
    end
end
