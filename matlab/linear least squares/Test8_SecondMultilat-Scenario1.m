%% Least squares method - Random
loss = [0;2;4;6;7;10;15];
testtype=loss;
gips=2;
figure(2)
hold on

testtype=loss';
ratio = zeros(realizations,length(testtype));
trilat = zeros(size(rssi_clean,1),2,monte,length(testtype));
correct = zeros(realizations,length(testtype));
ratio = zeros(realizations,length(testtype));
Y=[];
%multilat=7;
Yresult=[]
%Best at 6, going down with the value causes some bug
d0=0.5;
gamma=2.055;
rssi0=-41.98;
X=[];
esti_dist=[];
r=[];
A=[];
b=[];
multilat=5;
for z = 1:length(testtype)


    rssi = rssi_clean - (n.*loss(z)) + noise(:,:,:,1);
    
    for i =1:length(wall)
        esti_dist(i,:,:) =0.5/(exp(lambertw(0.05602396820*wall(i)*loss(z)*exp(-0.1120479364*rssi(i,:,:) - 4.703772370)) + 0.1120479364*rssi(i,:,:) + 4.703772370));
        %esti_dist(i,:,:) = 0.5*exp(-(10*lambertw(0.2302585092*wall(i)*exp(-0.2302585093*(rssi(i,:,:) + 41.98)/2.055)/2.055)*2.055 + 2.302585093*rssi(i,:,:) + 96.66252220)/(10*2.055));
    end
    for monte = 1:realizations
        for i = 1:size(rssi,1)
            [B,I] = maxk(rssi(i,:,monte),multilat);
            
            for k = 1:multilat
                X(k) = beaconx(I(k),monte);
                Y(k) = beacony(I(k),monte);
                r(k) = esti_dist(i,I(k),monte);
            end
            
            for k=1:multilat-1
                A(k,1)=2*[X(k+1) - X(1)]; %A matrix når x1,y1 ikke er sat i 0,0
                A(k,2)=2*[Y(k+1) - Y(1)];
                b(k,1)= [X(k+1)^2 + Y(k+1)^2 + r(1)^2 - X(1)^2 - Y(1)^2 - r(k+1)^2]; %B Matrix når x1,y1 ikke er sat i 0,0
            end
            
            trilat(i,:,monte,z) = (A'*A)\A'*b; %Least squares definition
            if monte == realizations
                scatter(trilat(i,1,monte),trilat(i,2,monte),'o','filled')
            end
            
            %viscircles([x1 y1],r1, 'Linewidth',1);
            %viscircles([x2 y2],r2, 'Linewidth',1);
            %viscircles([x3 y3],r3, 'Linewidth',1);
            
        end
        
        for j = 1:size(trilat,1)
            for i = 1:size(rssi,1)
                if trilat(j,1,monte,z)<=rooms(i,3) && trilat(j,1,monte,z)>=rooms(i,1) && trilat(j,2,monte,z)<=rooms(i,4) && trilat(j,2,monte,z)>=rooms(i,2)
                    if i == j
                        correct(monte,z)=correct(monte,z) + 1;
                    end
                    connection(j,monte,z) = i;
                    if monte == realizations
                        plot([sensorx(j,monte) beaconx(i,monte)],[sensory(j,monte) beacony(i,monte)], "--")
                    end
                end
            end
            
        end
        ratio(monte,z) = correct(monte,z)/(20);
    end
    Yresult(1,:)=sum(ratio)/realizations
end
plot(pgon)

%Plot walls
for i = 0:size(plan,1)-1
    p(i+1) = plot([plan(i+1,1) plan(i+1,3)],[plan(i+1,2) plan(i+1,4)],'k');
    p(i+1).LineWidth = plan(i+1,5);
end

%Add CU and sensors
scatter(beaconx(:,end),beacony(:,end))
scatter(sensorx(:,end),sensory(:,end), 'x')

%Plot axis
axis([-1 30 -1 30]);
axis equal
title('Least squares')
hold off

%% Least squares with outside room correction - Random
figure(3)
hold on
correct_corrected = zeros(realizations,length(testtype));
ratio_corrected = zeros(realizations,length(testtype));



room_dist = zeros(size(rssi,1),1);

x_center = ((rooms(:,3)-rooms(:,1))/2)+rooms(:,1);
y_center = ((rooms(:,4)-rooms(:,2))/2)+rooms(:,2);

connection2 = connection;
X=[];
esti_dist=[];
r=[];
A=[];
b=[];

for z=1:length(testtype)
    for monte = 1:realizations
        LS2_correct = zeros(1,length(testtype));
        for j = 1:size(trilat,1)
            if connection2(j,monte,z) == 0
                for i = 1:size(rssi,1)
                    room_dist(i) = sqrt((trilat(j,1,monte,z)-x_center(i))^2+(trilat(j,2,monte,z)-y_center(i))^2);
                end
                [M,I] = min(room_dist);
                connection2(j,monte,z) = I;
            end
        end
        
        for i = 1:size(rssi,1)
            if connection2(i,monte,z) == i
                LS2_correct(1,z)=LS2_correct(1,z)+1;
            end
            if monte == realizations
                plot([sensorx(i,monte) beaconx(connection2(i,monte),monte)],[sensory(i,monte) beacony(connection2(i,monte),monte)], "--")
                plot([trilat(i,1,monte) beaconx(connection2(i,monte),monte)],[trilat(i,2,monte) beacony(connection2(i,monte),monte)], "-")
                scatter(trilat(i,1,monte),trilat(i,2,monte),'o','filled')
            end
        end
        LS2_correct(1,z);
        ratio_corrected(monte,z) = LS2_correct(1,z)/(20);
    end
    Yresult(2,:)=sum(ratio_corrected)/realizations
end
plot(pgon)

%Plot walls
for i = 0:size(plan,1)-1
    p(i+1) = plot([plan(i+1,1) plan(i+1,3)],[plan(i+1,2) plan(i+1,4)],'k');
    p(i+1).LineWidth = plan(i+1,5);
end

%Add CU and sensors
scatter(beaconx(:,end),beacony(:,end))
scatter(sensorx(:,end),sensory(:,end), 'x')

%Plot axis
axis([-1 30 -1 30]);
axis equal
title('Least squares with room correction')
hold off


%% Least squares method - Center
figure(2)
hold on
trilat = zeros(size(rssi_clean,1),2,monte,length(testtype));
connection = zeros(size(rssi_clean,1),monte,length(testtype));
correct_center= zeros(realizations,length(testtype));
ratio_center = zeros(realizations,length(testtype));
X=[];
esti_dist=[];
r=[];
A=[];
b=[];
for z = 1:length(testtype)
    

    rssi = rssi_clean_center - (n_center.*loss(z)) + noise(:,:,:,1);
    
    for i =1:length(wall)
        esti_dist(i,:,:) =0.5/(exp(lambertw(0.05602396820*wall(i)*loss(z)*exp(-0.1120479364*rssi(i,:,:) - 4.703772370)) + 0.1120479364*rssi(i,:,:) + 4.703772370));
        %esti_dist(i,:,:) = 0.5*exp(-(10*lambertw(0.2302585092*wall(i)*exp(-0.2302585093*(rssi(i,:,:) + 41.98)/2.055)/2.055)*2.055 + 2.302585093*rssi(i,:,:) + 96.66252220)/(10*2.055));
    end

    for monte = 1:realizations
        for i = 1:size(rssi,1)
            [B,I] = maxk(rssi(i,:,monte),multilat);
            
            for k = 1:multilat
                X(k) = beaconx_center(I(k),monte);
                Y(k) = beacony_center(I(k),monte);
                r(k) = esti_dist(i,I(k),monte);
            end
            
            for k=1:multilat-1
                A(k,1)=2*[X(k+1) - X(1)]; %A matrix når x1,y1 ikke er sat i 0,0
                A(k,2)=2*[Y(k+1) - Y(1)];
                b(k,1)= [X(k+1)^2 + Y(k+1)^2 + r(1)^2 - X(1)^2 - Y(1)^2 - r(k+1)^2]; %B Matrix når x1,y1 ikke er sat i 0,0
            end
            
            trilat(i,:,monte,z) = (A'*A)\A'*b; %Least squares definition
            if monte == realizations && z==2
                scatter(trilat(i,1,monte,z),trilat(i,2,monte,z),'o','filled')
            end
            
            %viscircles([x1 y1],r1, 'Linewidth',1);
            %viscircles([x2 y2],r2, 'Linewidth',1);
            %viscircles([x3 y3],r3, 'Linewidth',1);
            
        end
        
        for j = 1:size(trilat,1)
            for i = 1:size(rssi,1)
                if trilat(j,1,monte,z)<=rooms(i,3) && trilat(j,1,monte,z)>=rooms(i,1) && trilat(j,2,monte,z)<=rooms(i,4) && trilat(j,2,monte,z)>=rooms(i,2)
                    if i == j
                        correct_center(monte,z)=correct_center(monte,z) + 1;
                    end
                    connection(j,monte,z) = i;
                    if monte == realizations && z==2
                        plot([sensorx(j,monte) beaconx_center(i,monte)],[sensory(j,monte) beacony_center(i,monte)], "--")
                    end
                end
            end
        end
        ratio_center(monte,z) = correct_center(monte,z)/(20);
    end
    Yresult(3,:)=sum(ratio_center)/realizations
end
plot(pgon)

%Plot walls
for i = 0:size(plan,1)-1
    p(i+1) = plot([plan(i+1,1) plan(i+1,3)],[plan(i+1,2) plan(i+1,4)],'k');
    p(i+1).LineWidth = plan(i+1,5);
end

%Add CU and sensors
scatter(beaconx_center(:,end),beacony_center(:,end))
scatter(sensorx(:,end),sensory(:,end), 'x')

%Plot axis
axis([-1 30 -1 30]);
axis equal
title('Least squares')
hold off

%% Least squares with outside room correction - center
figure(3)
hold on
correct_center_corrected = zeros(realizations,length(testtype));
ratio_center_corrected = zeros(realizations,length(testtype));
X=[];
esti_dist=[];
r=[];
A=[];
b=[];

room_dist = zeros(size(rssi,1),1);

x_center = ((rooms(:,3)-rooms(:,1))/2)+rooms(:,1);
y_center = ((rooms(:,4)-rooms(:,2))/2)+rooms(:,2);

connection2 = connection;

for z=1:length(testtype)
    for monte = 1:realizations
        LS2_correct = zeros(1,length(testtype));
        for j = 1:size(trilat,1)
            if connection2(j,monte,z) == 0
                for i = 1:size(rssi,1)
                    room_dist(i) = sqrt((trilat(j,1,monte,z)-x_center(i))^2+(trilat(j,2,monte,z)-y_center(i))^2);
                end
                [M,I] = min(room_dist);
                connection2(j,monte,z) = I;
            end
        end
        
        for i = 1:size(rssi,1)
            if connection2(i,monte,z) == i
                LS2_correct(1,z)=LS2_correct(1,z)+1;
            end
            if monte == realizations && z==2
                plot([sensorx(i,monte) beaconx(connection2(i,monte),monte)],[sensory(i,monte) beacony(connection2(i,monte),monte)], "--")
                plot([trilat(i,1,monte) beaconx(connection2(i,monte),monte)],[trilat(i,2,monte) beacony(connection2(i,monte),monte)], "-")
                scatter(trilat(i,1,monte),trilat(i,2,monte),'o','filled')
            end
        end
        ratio_center_corrected(monte,z) = LS2_correct(1,z)/(20);
    end
    Yresult(4,:)=sum(ratio_center_corrected)/realizations
end
plot(pgon)

%Plot walls
for i = 0:size(plan,1)-1
    p(i+1) = plot([plan(i+1,1) plan(i+1,3)],[plan(i+1,2) plan(i+1,4)],'k');
    p(i+1).LineWidth = plan(i+1,5);
end

%Add CU and sensors
scatter(beaconx(:,end),beacony(:,end))
scatter(sensorx(:,end),sensory(:,end), 'x')

%Plot axis
axis([-1 30 -1 30]);
axis equal
title('Least squares with room correction')
hold off

%% Least squares method - Corner
figure(2)
hold on
trilat = zeros(size(rssi_clean,1),2,monte,length(testtype));
connection = zeros(size(rssi_clean,1),monte,length(testtype));
correct_corner = zeros(realizations,length(testtype));
ratio_corner = zeros(realizations,length(testtype));
X=[];
esti_dist=[];
r=[];
A=[];
b=[];
for z = 1:length(testtype)
    
    rssi = rssi_clean_corner - (n_corner.*loss(z)) + noise(:,:,:,1);
    for i =1:length(wall)
        esti_dist(i,:,:) =0.5/(exp(lambertw(0.05602396820*wall(i)*loss(z)*exp(-0.1120479364*rssi(i,:,:) - 4.703772370)) + 0.1120479364*rssi(i,:,:) + 4.703772370));
        %esti_dist(i,:,:) = 0.5*exp(-(10*lambertw(0.2302585092*wall(i)*exp(-0.2302585093*(rssi(i,:,:) + 41.98)/2.055)/2.055)*2.055 + 2.302585093*rssi(i,:,:) + 96.66252220)/(10*2.055));
    end
    for monte = 1:realizations
        correct_corner = 0;
        for i = 1:size(rssi,1)
            [B,I] = maxk(rssi(i,:,monte),multilat);
            
            for k = 1:multilat
                X(k) = beaconx_corner(I(k),monte);
                Y(k) = beacony_corner(I(k),monte);
                r(k) = esti_dist(i,I(k),monte);
            end
            
            for k=1:multilat-1
                A(k,1)=2*[X(k+1) - X(1)]; %A matrix når x1,y1 ikke er sat i 0,0
                A(k,2)=2*[Y(k+1) - Y(1)];
                b(k,1)= [X(k+1)^2 + Y(k+1)^2 + r(1)^2 - X(1)^2 - Y(1)^2 - r(k+1)^2]; %B Matrix når x1,y1 ikke er sat i 0,0
            end
            
            trilat(i,:,monte,z) = (A'*A)\A'*b; %Least squares definition
            if monte == realizations && z==2
                scatter(trilat(i,1,monte,z),trilat(i,2,monte,z),'o','filled')
            end
            
            %viscircles([x1 y1],r1, 'Linewidth',1);
            %viscircles([x2 y2],r2, 'Linewidth',1);
            %viscircles([x3 y3],r3, 'Linewidth',1);
            
        end
        
        for j = 1:size(trilat,1)
            for i = 1:size(rssi,1)
                if trilat(j,1,monte,z)<=rooms(i,3) && trilat(j,1,monte,z)>=rooms(i,1) && trilat(j,2,monte,z)<=rooms(i,4) && trilat(j,2,monte,z)>=rooms(i,2)
                    if i == j
                        correct_corner=correct_corner + 1;
                    end
                    connection(j,monte,z) = i;
                    if monte == realizations && z==2
                        plot([sensorx(j,monte) beaconx_corner(i,monte)],[sensory(j,monte) beacony_corner(i,monte)], "--")
                    end
                end
            end
        end
        ratio_corner(monte,z) = correct_corner/(20);
    end
    Yresult(5,:)=sum(ratio_corner)/realizations
end
plot(pgon)

%Plot walls
for i = 0:size(plan,1)-1
    p(i+1) = plot([plan(i+1,1) plan(i+1,3)],[plan(i+1,2) plan(i+1,4)],'k');
    p(i+1).LineWidth = plan(i+1,5);
end

%Add CU and sensors
scatter(beaconx_corner(:,end),beacony_corner(:,end))
scatter(sensorx(:,end),sensory(:,end), 'x')

%Plot axis
axis([-1 30 -1 30]);
axis equal
title('Least squares')
hold off

%% Least squares with outside room correction - Corner
figure(3)
hold on
correct_corner_corrected = zeros(realizations,length(testtype));
ratio_corner_corrected = zeros(realizations,length(testtype));
room_dist = zeros(size(rssi,1),1);

x_center = ((rooms(:,3)-rooms(:,1))/2)+rooms(:,1);
y_center = ((rooms(:,4)-rooms(:,2))/2)+rooms(:,2);

connection2 = connection;
X=[];
esti_dist=[];
r=[];
A=[];
b=[];

for z=1:length(testtype)
    for monte = 1:realizations
        LS2_correct = zeros(1,length(testtype));
        for j = 1:size(trilat,1)
            if connection2(j,monte,z) == 0
                for i = 1:size(rssi,1)
                    room_dist(i) = sqrt((trilat(j,1,monte,z)-x_center(i))^2+(trilat(j,2,monte,z)-y_center(i))^2);
                end
                [M,I] = min(room_dist);
                connection2(j,monte,z) = I;
            end
        end
        
        for i = 1:size(rssi,1)
            if connection2(i,monte,z) == i
                LS2_correct(1,z)=LS2_correct(1,z)+1;
            end
            if monte == realizations
                plot([sensorx(i,monte) beaconx(connection2(i,monte),monte)],[sensory(i,monte) beacony(connection2(i,monte),monte)], "--")
                plot([trilat(i,1,monte) beaconx(connection2(i,monte),monte)],[trilat(i,2,monte) beacony(connection2(i,monte),monte)], "-")
                scatter(trilat(i,1,monte),trilat(i,2,monte),'o','filled')
            end
        end
        LS2_correct(1,z);
        ratio_corner_corrected(monte,z) = LS2_correct(1,z)/(20);
    end
    Yresult(6,:)=sum(ratio_corner_corrected)/realizations
end
plot(pgon)

%Plot walls
for i = 0:size(plan,1)-1
    p(i+1) = plot([plan(i+1,1) plan(i+1,3)],[plan(i+1,2) plan(i+1,4)],'k');
    p(i+1).LineWidth = plan(i+1,5);
end

%Add CU and sensors
scatter(beaconx(:,end),beacony(:,end))
scatter(sensorx(:,end),sensory(:,end), 'x')

%Plot axis
axis([-1 30 -1 30]);
axis equal
title('Least squares with room correction')
hold off





%% Plotting and etc.
Noisetesting = 1;
Noisetesting_corrected=1;

%Average room size clac below, for sanity
room_size = mean((rooms(:,3)-rooms(:,1)).*(rooms(:,4)-rooms(:,2)));

yaxis_random=sum(ratio);
yaxis_center=sum(ratio_center);
yaxis_corner=sum(ratio_corner);
% Use Y if for both at once

z = 2.576; % equal to 95% interval

sigma = std(ratio);
sigma_center = std(ratio_center);
sigma_corner = std(ratio_corner);
sigma_corrected = std(ratio_corrected);
sigma_center_corrected = std(ratio_center_corrected);
sigma_corner_corrected = std(ratio_corner_corrected);
%calc lower

lower = mean(ratio)-z*(sigma/sqrt(realizations));
lower_center = mean(ratio_center)-z*(sigma_center/sqrt(realizations));
lower_corner = mean(ratio_corner)-z*(sigma_corner/sqrt(realizations));
lower_corrected = mean(ratio_corrected)-z*(sigma_corrected/sqrt(realizations));
lower_center_corrected = mean(ratio_center_corrected)-z*(sigma_center_corrected/sqrt(realizations));
lower_corner_corrected = mean(ratio_corner_corrected)-z*(sigma_corner_corrected/sqrt(realizations));

%calc upper
upper = mean(ratio)+z*(sigma/sqrt(realizations));
upper_center = mean(ratio_center)+z*(sigma_center/sqrt(realizations));
upper_corner= mean(ratio_corner)+z*(sigma_corner/sqrt(realizations));

upper_corrected = mean(ratio_corrected)+z*(sigma_corrected/sqrt(realizations));
upper_center_corrected = mean(ratio_center_corrected)+z*(sigma_center_corrected/sqrt(realizations));
upper_corner_corrected = mean(ratio_corner_corrected)+z*(sigma_corner_corrected/sqrt(realizations));




if Noisetesting==1
    if realizations > 1
        figure(12)
        hold on
        
        yy = [lower,fliplr(upper)];   % vector of upper & lower boundaries
        fill([testtype,fliplr(testtype)],yy,'b','edgealpha',0,'facealpha',.3)
        
        yy = [lower_center,fliplr(upper_center)];   % vector of upper & lower boundaries
        fill([testtype,fliplr(testtype)],yy,'r','edgealpha',0,'facealpha',.3)
        
        yy = [lower_corner,fliplr(upper_corner)];   % vector of upper & lower boundaries
        fill([testtype,fliplr(testtype)],yy,'m','edgealpha',0,'facealpha',.3)
        
        %plot mean
        plot(testtype,mean(ratio),'b-o')
        plot(testtype,mean(ratio_center),'r-o')
        plot(testtype,mean(ratio_corner),'m-o')
        %xline(22.159,'--');
        xlim([0 multilatmax(end)])
        ylim([0 1])
        legend('Random','Center','Corner')
        title('Accuracy ratio over variance of noise')
        xlabel('Number of devices used for multilateration')
        ylabel('Accuracy ratio')
        grid
    end
end


if Noisetesting_corrected==1
    if realizations > 1
        figure(14)
        hold on
        
        yy = [lower_corrected,fliplr(upper_corrected)];   % vector of upper & lower boundaries
        fill([testtype, fliplr(testtype)],yy,'b','edgealpha',0,'facealpha',.3)
        
        yy = [lower_center_corrected,fliplr(upper_center_corrected)];   % vector of upper & lower boundaries
        fill([testtype, fliplr(testtype)],yy,'r','edgealpha',0,'facealpha',.3)
        
        yy = [lower_corner_corrected,fliplr(upper_corner_corrected)];   % vector of upper & lower boundaries
        fill([testtype, fliplr(testtype)],yy,'m','edgealpha',0,'facealpha',.3)
        
        %plot mean
        plot(testtype,mean(ratio_corrected),'b-o')
        plot(testtype,mean(ratio_center_corrected),'r-o')
        plot(testtype,mean(ratio_corner_corrected),'m-o')
        xline(22.159,'--');
        xlim([0 testtype(end)])
        ylim([0 1])
        legend('Random corrected','Center corrected','Corner corrected')
        title('Accuracy ratio over dB loss')
        xlabel('dB loss per wall')
        ylabel('Accuracy ratio')
        grid
    end
end
