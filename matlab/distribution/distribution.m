clc
clear all
close all
fd = csvread('xy.csv');

lfd = length(fd);

%X=zeros(lfd);

figure(8)
scatter(fd(:,1),fd(:,2))
legend('Mean of measured data')
xlabel('Length in meters')
ylabel('RSSI in dBm')
title('Measured RSSI')
for i = 1:lfd
    X(i)=fd(i,2)-((-41.9813)+10*(-2.055)*log10(fd(i,1)/0.5));
end

sigma=var(X);

lpdf = -20:0.25:20;

Y=normpdf(lpdf,0,sqrt(sigma));

figure(1)
histogram(X)
%hold on
figure(2)
plot(lpdf,Y)
%hold off
[h,p] = chi2gof(X,'Alpha',0.001)
%0.5*exp((-0.2302585093 * (fd(i,1)-1*(-41.9813)))/2.317)