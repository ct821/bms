
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include "esp_sleep.h"

BLEAdvertising *adv;

void setup() {
  Serial.begin(115200);
  BLEDevice::init("");
  Serial.println("Enabled BLE device");
  
  adv = BLEDevice::getAdvertising();

  BLEAdvertisementData adv_data1 = BLEAdvertisementData();
  std::string strServiceData1 = "";
  adv_data1.addData(strServiceData1);
  adv->setScanResponseData(adv_data1);

  int channels[] = {1, 2, 4};
  uint16_t interval = 0x20;


  BLEAdvertisementData adv_data = BLEAdvertisementData();
  std::string strServiceData = "";

  strServiceData += (char)0x05;     // Len
  strServiceData += (char)channels[0];     
  strServiceData += (char)channels[0];     
  strServiceData += (char)channels[0];   
  strServiceData += (char)channels[0];     
  strServiceData += (char)channels[0];       

  adv_data.addData(strServiceData);
  adv->setAdvertisementData(adv_data);
  adv->setMinInterval(interval);
  adv->setMaxInterval(interval);
  
  adv->start();
  
  while (1){
    for (int i = 0; i < 3; i++) {
      BLEAdvertisementData adv_data = BLEAdvertisementData();
      std::string strServiceData = "";
    
      strServiceData += (char)0x05;     // Len
      strServiceData += (char)channels[i];     
      strServiceData += (char)channels[i];     
      strServiceData += (char)channels[i];   
      strServiceData += (char)channels[i];     
      strServiceData += (char)channels[i];       
    
      adv_data.addData(strServiceData);
      adv->setAdvertisementData(adv_data);
      
      //adv->start();
      Serial.print("Advertizing: ");
      Serial.println(strServiceData.c_str());
      Serial.println(interval);
      delay(1000);
      //adv->stop();
      //delay(100);
    }
  }
}

void loop() {
   // Start advertising
}
