#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <dirent.h>

static const char RASP4_1[] = "dc:a6:32:82:34:61";	// List of known MAC addresses
static const char RASP4_2[] = "dc:a6:32:49:0a:9e";
static const char RASP4_3[] = "dc:a6:32:86:90:d0";
static const char RASP4_4[] = "dc:a6:32:86:96:79";
static const char RASP4_5[] = "dc:a6:32:86:90:be";
static const char RASP4_6[] = "dc:a6:32:86:90:f1";

static const char RASP3_3[] = "b8:27:eb:62:af:40";
static const char RASP3_4[] = "b8:27:eb:13:7e:c8";



int main(int argc, char *argv[]) {
	time_t begin = time(NULL);

	if(argc != 2) {
		printf("You need to specify an input directory\n");
		return 1;
	}

	DIR *dir;
	struct dirent *entry;

	if(!(dir = opendir(argv[1]))) {
        	return 0;
	}

	while((entry = readdir(dir)) != NULL) {
		int pathlen = strlen(argv[1]);
		int len = strlen(entry->d_name);
		const char *endchar = &entry->d_name[len-4];
		if(entry->d_type != DT_DIR && !strcmp(endchar,".txt")) {
			char *initinfile;
			char *infile;
			if((initinfile = malloc((pathlen + len + 4)*sizeof(char)))!=NULL) {
				infile = initinfile;
				memset(infile,'\0',sizeof(infile));
				strcat(infile,"./");
				strcat(infile,argv[1]);
				strcat(infile,"/");
				strcat(infile,entry->d_name);
			} else {
 				printf("malloc failed!\n");
				return 0;
			}
			printf("%s\n",infile);
			FILE *ftmp = fopen(infile, "r");
			if(!ftmp) {
				printf("Can't open input file\n");
			}
			size_t out_file_name_len = strlen(infile);
			printf("%d\n",(int)out_file_name_len);
			char newname[strlen(infile)];
			//if((newname=malloc())!=NULL){
				memset(newname,'\0',sizeof(newname));
				
				strncpy(newname, infile, strlen(infile)-4);
				printf("new name: %s\n",newname);
				strcat(newname,".csv");
				
			//}else {
 			//   printf("malloc failed!\n");
			//	return 0;
			//}

			
			FILE *fcsv = fopen(newname, "w+");
			if(!fcsv) {
				printf("Can't create or open output file\n");
				return 1;
			}
			free(initinfile);
			infile = NULL;
			//free(newname);
			//newname = NULL;

			char *line = NULL; //pointer to use with getline()
			ssize_t nread = 0; //characters read by getline()
			size_t len = 0;    //number of bytes to allocate

			char *token;
			int col_count = 0; //counter for column in line
			int row_count = 0; //counter for rows / lines

			while((nread = getline(&line, &len, ftmp)) != -1) {
				token = strtok(line, ",");
				col_count = 0;
				row_count++;

				while(token != NULL) {
					if(col_count == 0) {				// TIME in seconds
						fputs(token, fcsv);
						fputc(',', fcsv);
					}
					if(col_count == 1) {				// Replace MAC with number
						if(strcmp(RASP4_1, token) == 0) {	// RASP4_1 ==> 1
							fputs("1,", fcsv);
						} else if(strcmp(RASP4_2, token) == 0) {// RASP4_2 ==> 2
							fputs("2,", fcsv);
						} else if(strcmp(RASP4_3, token) == 0) {// RASP4_3 ==> 3
							fputs("3,", fcsv);
						} else if(strcmp(RASP4_4, token) == 0) {// RASP4_4 ==> 4
							fputs("4,", fcsv);
						} else if(strcmp(RASP4_5, token) == 0) {// RASP4_5 ==> 5
							fputs("5,", fcsv);
						} else if(strcmp(RASP4_6, token) == 0) {// RASP4_6 ==> 6
							fputs("6,", fcsv);
						} else if(strcmp(RASP3_3, token) == 0) {// RASP3_3 ==> 7
							fputs("7,", fcsv);
						} else if(strcmp(RASP3_4, token) == 0) {// RASP3_4 ==> 8
							fputs("8,", fcsv);
						} else {				// All unknown ==> 0
							fputs("0,", fcsv);
						}
					}
					if(col_count == 2) {				// RSSI
						fputs(token, fcsv);
						fputc(',', fcsv);
					}
					if(col_count == 3) {				// Connectable = 1, non connectable = 0
						fputs(token, fcsv);
						fputc(',', fcsv);
					}
					if(col_count == 4) {				// Ch37 = 1, Ch38 = 2, Ch39 = 4
						fputs(token, fcsv);
						//fputc(',', fcsv);
					}
					token = strtok(NULL, ",");
					col_count++;
				}
				free(line);
				line = NULL;
			}
			free(line);
			free(token);
			fclose(fcsv);
			fclose(ftmp);
			remove("tmp.txt");
			time_t end = time(NULL);
			printf("# of lines: %d \n", row_count);
			printf("Time spend: %ld seconds\n", (end - begin));
		}
		
	}
	closedir(dir);
	return 0;
}

