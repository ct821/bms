function varargout = new_split_data(varargin)

mac_arg_counter = 0;
for k = 1:nargin
    if mod(varargin{k}, 1) == 0
        mac_arg_counter = mac_arg_counter + 1;
    end
end
log_arg_counter = nargin - mac_arg_counter;

for ii = 1:log_arg_counter
    for jj = 1:mac_arg_counter
        array{ii, jj} = [];
        array_counter{ii, jj} = 1;
    end
end

for c_log = 1:log_arg_counter                 % looper gennem alle logs
    for c_line = 1:length(varargin{c_log})    % looper gennem alle linier i hver log fil
        for c_mac = 1:mac_arg_counter         % looper for hver mac der er angivet
            if varargin{c_log}(c_line, 2) == varargin{c_mac + log_arg_counter} && varargin{c_log}(c_line, 4) == 1              
                array{c_log, c_mac}(array_counter{c_log, c_mac}, :) = varargin{c_log}(c_line, :);
                array_counter{c_log, c_mac} = array_counter{c_log, c_mac} + 1;
            end
        end
    end
end
varargout{1} = array;
%varargout{2} = mac_arg_counter;
%varargout{3} = log_arg_counter;
%varargout{4} = array_counter;
end