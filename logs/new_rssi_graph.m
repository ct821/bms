function varargout = new_rssi_graph(varargin)

[test_max, mac_max] = size(varargin{1});
array_max{test_max, mac_max} = [];
array_min{test_max, mac_max} = [];
array_mean{test_max, mac_max} = [];

fpath = sprintf('%s/figures', pwd);

for mac_c = 1:mac_max
    for test_c = 1:test_max
        chtot_c = 1;
        ch37_c = 1;
        ch38_c = 1;
        ch39_c = 1;
        
        y0 = [];
        y1 = [];
        y2 = [];
        y3 = [];

        for i = 1:length(varargin{1}{test_c, mac_c})
            y0(chtot_c, 1) = varargin{1}{test_c, mac_c}(i, 1);
            y0(chtot_c, 2) = varargin{1}{test_c, mac_c}(i, 3);
            chtot_c = chtot_c + 1;
            
            if varargin{1}{test_c, mac_c}(i, 5) == 1
                y1(ch37_c, 1) = varargin{1}{test_c, mac_c}(i, 1);
                y1(ch37_c, 2) = varargin{1}{test_c, mac_c}(i, 3);
                ch37_c = ch37_c + 1;
            elseif varargin{1}{test_c, mac_c}(i, 5) == 2
                y2(ch38_c, 1) = varargin{1}{test_c, mac_c}(i, 1);
                y2(ch38_c, 2) = varargin{1}{test_c, mac_c}(i, 3);
                ch38_c = ch38_c + 1;
            else
                y3(ch39_c, 1) = varargin{1}{test_c, mac_c}(i, 1);
                y3(ch39_c, 2) = varargin{1}{test_c, mac_c}(i, 3);
                ch39_c = ch39_c + 1;
            end
        end
        
        if isempty(y0) == 0 && isempty(y1) == 0 && isempty(y2) == 0 && isempty(y3) == 0
            fname = sprintf('RPi4-%d_%.01f-m_log_%d', mac_c, varargin{2}(test_c, 1), test_c - 1);
            
            set(0,'DefaultFigureVisible','off');
            if isfile(fullfile(fpath, fname))
                % File exist, do nothing
            else
                figure();
                plot(y1(:,1), y1(:,2),'b', y2(:,1), y2(:,2),'r', y3(:,1), y3(:,2),'g');
                ylim([-100 0])
                title(sprintf('RSSI over time for RPi4-%d for %.01f m (%s%d)', mac_c, varargin{2}(test_c, 1), 'log\_', test_c - 1))
                xlabel('Relative time in seconds (s)')
                ylabel('RSSI (dBm)')
                legend('Ch37', 'Ch38', 'Ch39')
                saveas(gcf, fullfile(fpath, fname), 'png');
                close(gcf)
            end
            set(0,'DefaultFigureVisible','on');

            array_max{test_c, mac_c} = [max(y1(:,2))    % Ch37
                                        max(y2(:,2))    % Ch38
                                        max(y3(:,2))];  % Ch39

            array_min{test_c, mac_c} = [min(y1(:,2))    % Ch37
                                        min(y2(:,2))    % Ch38
                                        min(y3(:,2))];  % Ch39

            array_mean{test_c, mac_c} = [mean(y1(:,2))    % Ch37
                                         mean(y2(:,2))    % Ch38
                                         mean(y3(:,2))];  % Ch39
        end
    end
end

varargout{1} = array_max;
varargout{2} = array_min;
varargout{3} = array_mean;

end