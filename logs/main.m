%% Reset
clear all
close all
clc

%% Clear some variables
clear data_max data_min data_mean
close all
clc

%% Import files and define mac
folder = {dir('**/pathloss_measure_jeppe/*.csv'); dir('**/pathloss_measure_tati/*.csv'); dir('**/pathloss_measure_tati_wall/*.csv')}; %**/pathloss_test_wall_compare/*.csv
info_files = {dir('**/pathloss_measure_jeppe/info'); dir('**/pathloss_measure_tati/info'); dir('**/pathloss_measure_tati_wall/info')};

for i = 1:length(folder)
    [~, reindex] = sort( str2double( regexp( {folder{i, 1}.name}, '\d+', 'match', 'once' ))); % Sort files in ascending order instead of per character
    folder{i, 1} = folder{i, 1}(reindex);
    files{i, 1}{length(folder{i, 1}), 1} = [];
    
    for file_c = 1:length(folder{i,1})
        files{i, 1}{file_c, 1} = csvread(sprintf('%s/%s', folder{i, 1}(file_c).folder, folder{i, 1}(file_c).name));
    end
    
    fd = fopen(sprintf('%s/%s', info_files{i, 1}.folder, info_files{i, 1}.name));
    dist{i, 1} = textscan(fd, '%f%f', 'HeaderLines', 1, 'Delimiter', ',');
    fclose(fd);
    
end

mac1 = 1; % dc:a6:32:82:34:61 <==> pi 4-1
mac2 = 2; % dc:a6:32:49:0a:9e <==> pi 4-2
mac3 = 3; % dc:a6:32:86:90:d0 <==> pi 4-3
mac4 = 4; % dc:a6:32:86:96:79 <==> pi 4-4
mac5 = 5; % dc:a6:32:86:90:be <==> pi 4-5
mac6 = 6; % dc:a6:32:86:90:f1 <==> pi 4-6
mac7 = 7; % b8:27:eb:62:af:40 <==> pi 3-3
mac8 = 8; % b8:27:eb:13:72:c8 <==> pi 3-4

%% Split data structure, filtered unnecessary data, rows = test, columns = mac
data_jep = new_split_data(files{1, 1}{1:end,1}, mac1, mac2, mac3, mac4, mac5, mac6);
data_tat = new_split_data(files{2, 1}{1:end,1}, mac1, mac2, mac3, mac4, mac5, mac6);
data_tat_wall = new_split_data(files{3, 1}{1:end,1}, mac1, mac2, mac3, mac4, mac5, mac6);

%% Data for max, min, and mean rssi for each test, [Ch37,Ch38,Ch39]
[data_jep_max, data_jep_min, data_jep_mean] = new_rssi_graph(data_jep, dist{1, 1}{1, 2});
[data_tat_max, data_tat_min, data_tat_mean] = new_rssi_graph(data_tat, dist{2, 1}{1, 2});
[data_tat_wall_max, data_tat_wall_min, data_tat_wall_mean] = new_rssi_graph(data_tat_wall, dist{3, 1}{1, 2});

%%
data = [data_jep;data_tat];
data_mean = [data_jep_mean;data_tat_mean];
[j, k] = size(data);
for jj = 1:j
    for kk = 1:k
        if isempty(data{jj,kk}) == 0
            if isempty(data_mean{jj,kk}) == 0
                tot(jj, kk) = mean(data_mean{jj, kk});
            end
            if isempty(data_mean{jj,kk}) == 0
                ch37(jj, kk) = data_mean{jj, kk}(1, 1);
            end
            if isempty(data_mean{jj,kk}) == 0
                ch38(jj, kk) = data_mean{jj, kk}(2, 1);
            end
            if isempty(data_mean{jj,kk}) == 0
                ch39(jj, kk) = data_mean{jj, kk}(3, 1);
            end
            if isempty(data_mean{jj,kk}) == 0
                best(jj, kk) = max(data_mean{jj, kk}(:, 1));
            end
        end
    end
end

%%
xakse1 = [dist{1,1}{1,2} dist{1,1}{1,2} dist{1,1}{1,2} dist{1,1}{1,2} dist{1,1}{1,2}];
xakse2 = [dist{2,1}{1,2} dist{2,1}{1,2} dist{2,1}{1,2} dist{2,1}{1,2} dist{2,1}{1,2}];     
xakse = [xakse1;xakse2];     

     a=reshape(ch37(1:end,2:end).',1,[]);
     aa=reshape(ch38(1:end,2:end).',1,[]);
     aaa=reshape(ch39(1:end,2:end).',1,[]);
     aaaa=reshape(tot(1:end,2:end).',1,[]);
     aaaaa=reshape(best(1:end,2:end).',1,[]);
     b=reshape(xakse.',1,[]);
        
%%
figure()
%scatter(xakse,ch37)
scatter(b,a)
lsline
xlim([0 20.5])
ylim([-100 0])
set(gca,'xscale','log')
title('RSSI for CH37 for different distances')
xlabel('Distance (m)')
ylabel('RSSI (dBm)')
legend('Ch37')


figure()
%scatter(xakse,ch38)
scatter(b,aa)
lsline
xlim([0 20.5])
ylim([-100 0])
title('RSSI for CH38 for different distances')
set(gca,'xscale','log')
xlabel('Distance (m)')
ylabel('RSSI (dBm)')
legend('Ch38')


figure()
%scatter(xakse,ch39)
scatter(b,aaa)
lsline
xlim([0 20.5])
ylim([-100 0])
title('RSSI for CH39 for different distances')
set(gca,'xscale','log')
xlabel('Distance (m)')
ylabel('RSSI (dBm)')
legend('Ch39')


figure()
%scatter(xakse,tot)
scatter(b,aaaa)
lsline
xlim([0 20.5])
ylim([-100 0])
title('RSSI for ALL CH for different distances')
set(gca,'xscale','log')
xlabel('Distance (m)')
ylabel('RSSI (dBm)')
legend('ALL Ch')


figure()
%scatter(xakse,best)
scatter(b,aaaaa)
lsline
xlim([0 20.5])
ylim([-100 0])
title('RSSI for BEST CH for different distances')
set(gca,'xscale','log')
xlabel('Distance (m)')
ylabel('RSSI (dBm)')
legend('Best Ch')

%figure()
%boxplot([ch37(1:10,1) ch37(11:20,1) ch37(21:30,1) ch37(31:40,1) ch37(41:50,1) ch37(51:60,1) ch37(61:70,1) ch37(71:80,1) ch37(81:90,1) ch37(91:100,1)])

%figure()
%boxplot([ch38(1:10,1) ch38(11:20,1) ch38(21:30,1) ch38(31:40,1) ch38(41:50,1) ch38(51:60,1) ch38(61:70,1) ch38(71:80,1) ch38(81:90,1) ch38(91:100,1)])

%figure()
%boxplot([ch39(1:10,1) ch39(11:20,1) ch39(21:30,1) ch39(31:40,1) ch39(41:50,1) ch39(51:60,1) ch39(61:70,1) ch39(71:80,1) ch39(81:90,1) ch39(91:100,1)])

%figure()
%boxplot([tot(1:10,1) tot(11:20,1) tot(21:30,1) tot(31:40,1) tot(41:50,1) tot(51:60,1) tot(61:70,1) tot(71:80,1) tot(81:90,1) tot(91:100,1)])

%figure()
%boxplot([best(1:10,1) best(11:20,1) best(21:30,1) best(31:40,1) best(41:50,1) best(51:60,1) best(61:70,1) best(71:80,1) best(81:90,1) best(91:100,1)])



