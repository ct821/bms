#!/usr/bin/env python3
from __future__ import print_function

import sys
import time
import os
from bluepy import btle

if os.getenv('C', '1') == '0':
    ANSI_RED = ''
    ANSI_GREEN = ''
    ANSI_YELLOW = ''
    ANSI_CYAN = ''
    ANSI_WHITE = ''
    ANSI_OFF = ''
else:
    ANSI_CSI = "\033["
    ANSI_RED = ANSI_CSI + '31m'
    ANSI_GREEN = ANSI_CSI + '32m'
    ANSI_YELLOW = ANSI_CSI + '33m'
    ANSI_CYAN = ANSI_CSI + '36m'
    ANSI_WHITE = ANSI_CSI + '37m'
    ANSI_OFF = ANSI_CSI + '0m'

fileid = 0
while os.path.exists("log_%s.txt" % fileid):
    fileid += 1
file = open("log_%s.txt" % fileid,"a")
ref_time = time.time()


class ScanDelegate(btle.DefaultDelegate):
    def handleDiscovery(self, dev, isNewDev, isNewData):
        print(ANSI_CYAN + str(time.time()-ref_time) + ANSI_OFF, end=': ')

        print(dev.addr, end=', ')

        print('rssi: ' + ANSI_RED+str(dev.rssi) + ANSI_OFF, end=', ')

        print ('data: ', end=ANSI_YELLOW+'')

        data = [0] * len(dev.rawData)
        for i in range(len(dev.rawData)):
            data[i] = str(dev.rawData[i])
            print(data[i], end=' ')
        print(ANSI_OFF)

        sys.stdout.flush()

        if len(data) == 0:
            data = '0'


        #address = int(dev.addr.replace(":", ""), 16)
        #str1 = str(time.time()-ref_time) + ',' + str(address) + ',' + str(dev.rssi) + ',' + str(int(dev.connectable)) + ',' + (data[0])+'\n'

        str1 = str(time.time()-ref_time) + ',' + str(dev.addr) + ',' + str(dev.rssi) + ',' + str(int(dev.connectable)) + ',' + (data[0])+'\n'
        file.write(str1)


def main():
    scanner = btle.Scanner().withDelegate(ScanDelegate())
    scanner.scan(120, passive=True)

if __name__ == "__main__":
    main()