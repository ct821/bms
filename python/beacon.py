#!/usr/bin/env python3
"""
Simple BLE advertisement example
"""
from time import sleep
import bluetooth._bluetooth as bluez

from bluetooth_utils import (toggle_device, start_le_advertising,
                             stop_le_advertising)

dev_id = 0  # the bluetooth device is hci0
toggle_device(dev_id, True)

channels = (1, 2, 4)
x = 1

try:
    sock = bluez.hci_open_dev(dev_id)
except:
    print("Cannot open bluetooth device %i" % dev_id)
    raise

try:
    while True:
        print(x)
        x += 1
        for i in range(3):
            start_le_advertising(sock, min_interval=0x0020, max_interval=0x0020, data=(channels[i], channels[i], channels[i]) + (0,) * 27 + (69,), channel=channels[i])
            sleep(2)
            stop_le_advertising(sock)
            sleep(0.1)
except:
    stop_le_advertising(sock)
    raise